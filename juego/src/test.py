import pygame
import pygame.camera
from pygame.locals import *

DEVICE = '/dev/video0'
SIZE = (1920, 1080)
FILENAME = 'capture.png'

display = pygame.display.set_mode(SIZE, 0)
screen = pygame.surface.Surface(SIZE, 0, display)

def text(text, x, y, color=(0,0,0), size=30, font='Calibri'): # blits text to the screen
    text = str(text)

    font = pygame.font.SysFont(font, size)
    text = font.render(text, True, color)

    screen.blit(text, (x, y))


def camstream():
    pygame.init()
    pygame.camera.init()
    pygame.time.set_timer(pygame.USEREVENT, 1000)
    frames = 0 # counts number of frames for every second
    fps = 0
    camera = pygame.camera.Camera(DEVICE, SIZE)
    camera.start()
    capture = True
    while capture:

        cam = camera.get_image(screen)
        display.blit(cam, (0,0))
        pygame.display.flip()
        frames += 1
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                quit()
            if event.type == pygame.USEREVENT: # updates fps every second
                fps = frames
                print(fps)
                frames = 0 # reset frame count
        text(fps, 0, 0)
    camera.stop()
    pygame.quit()
    return

if __name__ == '__main__':
    camstream()
