export class Pattern {
    id: number;
    type: string;
    color: string;
    perimeter: any;
}

export class Query {
    patterns: Pattern[];
    frameImage: any;

    constructor() {
        this.patterns = [];
    }
}
