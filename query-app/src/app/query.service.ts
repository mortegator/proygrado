import { Injectable, Output, EventEmitter } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Pattern, Query } from './pattern';
import { v4 as uuid } from 'uuid';
import { SearchService } from './search.service';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class QueryService {
  queryColors = [
      'rgba(255, 0, 0, 0.5)','rgba(0, 255, 0, 0.5)',
      'rgba(0, 0, 255, 0.5)','rgba(125, 125, 0, 0.5)',
      'rgba(125, 125, 0, 0.5)'];

  public query = new BehaviorSubject<Query>(new Query());
  public query$ = this.query.asObservable();

  public numPatterns = 0;
  constructor(
      private http: HttpClient,
      private searchService: SearchService) {
      this.init();
  }

  init(): void {
      this.http.get("http://127.0.0.1:5000/scene/1", {responseType: 'text'})
        .subscribe((data) => {
              var newQuery = this.query.getValue();
              newQuery.frameImage = data;
              this.query.next(newQuery);
      });
      // this.http.get("http://127.0.0.1:5000/init_info")
      //   .subscribe((data) => {
      //       this.initInfo.next(data as any);
      //       console.log("starteddd");
      //   })
  }

  getNewColor() {
      return this.queryColors[this.query.getValue().patterns.length];
  }

  removePattern(pattern) {
      var newQuery = this.query.getValue();
      newQuery.patterns = this.query.getValue().patterns.filter(p => p.id != pattern.id);
      this.query.next(newQuery);
  }

  addPolygon(perimeter, color) {
      let new_pattern = new Pattern();
      new_pattern.id = this.query.getValue().patterns.length +1;
      new_pattern.type = 'polygon';
      new_pattern.color = color;
      new_pattern.perimeter = perimeter;
      var newQuery = this.query.getValue();
      newQuery.patterns.push(new_pattern);
      this.query.next(newQuery);
  }

  doSearch() {
      // Servicio de busquedas se encarga de crear la busqueda
      this.searchService.newSearch(this.query.getValue());
      // Empieza nueva busqueda
      // Salva frame imagen base
      var frameImage = this.query.getValue().frameImage;
      var newQuery = new Query();
      newQuery.frameImage= frameImage;
      this.query.next(newQuery);
  }
}
