import { Component, OnInit, Input,ViewChild} from '@angular/core';
import { Search } from '../search';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { HttpClient } from '@angular/common/http';
import { SearchService } from '../search.service';
import { Options, LabelType } from 'ng5-slider';
import * as Highcharts from 'highcharts/';
import * as HC_histogram from 'highcharts/modules/histogram-bellcurve';
import {formatDate } from '@angular/common';

HC_histogram(Highcharts);

@Component({
  selector: 'result-list',
  templateUrl: './result-list.component.html',
  styleUrls: ['./result-list.component.css']
})
export class ResultListComponent implements OnInit {
    search: Search;
    frameSelected: number;
    matSlider: any;
    Highcharts = Highcharts;
    chartOptions = null;

    minFilterValue: number;
    maxFilterValue: number;
    options: Options = {
        floor: 0,
        ceil: 100,
        step: 1
    };
// some rangom shitr

    minFilterTimestampValue: number;
    maxFilterTimestampValue: number;
    optionsTimestamp: Options = {
        floor: 0,
        ceil: 100,
        step: 1,
        translate: (value: number, label: LabelType): string => {
         return formatDate(new Date(value*1000), 'dd-MM-yyyy hh:mm:ss',  'en-US', '-0300');
       }
    };

  constructor(
      private http: HttpClient,
      public searchService: SearchService
  ) { }


  ngOnInit() {
      this.searchService.activeSearch$.subscribe(search => {
        if (this.search && this.search.localId == search.localId) {
            return;
        }
        this.search = search;
        if (search.results) {
            // Filtros y histograma de permanencia
            var permanencias =  search.results.people.map(p=> p.duracion);
            this.chartOptions=  {
                'title': {
                    'text': 'Permanencia'
                },
                'xAxis': [ {
                    'title': { 'text': 'Minutos' },
                    'alignTicks': false,
                }],
                'yAxis': [ {
                    'title': { 'text': '# personas' },
                }],
                'legend': {
                    'enabled': false
                },
                'series': [{
                        'name': 'Histogram',
                        'type': 'histogram',
                        'baseSeries': 's1'
                    }, {
                        'id': 's1',
                        'data': permanencias,
                        'name': 'Data',
                       'type': 'scatter',
                       'visible': false,
                       'marker': {
                           'radius': 1.5
                       }
                    }]
            }
            this.options.floor = Math.min(...permanencias);
            this.options.ceil  = Math.max(...permanencias);
            this.minFilterValue = this.options.floor;
            this.maxFilterValue = this.options.ceil;

            // Filtros de tiempo
            var ts_starts =  search.results.people.map(p=> p.timestamp_start);
            var ts_ends =  search.results.people.map(p=> p.timestamp_end);
            this.optionsTimestamp.floor = Math.round(Math.min(...ts_starts));
            this.optionsTimestamp.ceil  = Math.round(Math.max(...ts_ends));
            this.minFilterTimestampValue = this.optionsTimestamp.floor;;
            this.maxFilterTimestampValue = this.optionsTimestamp.ceil;
        }
      });
  }

  public showPose(person) {
      this.search.results.people.forEach( r => r.mark_draw = false);
      person.mark_draw = true;
      this.searchService.activeSearch.next(this.search);
  }

  public verFrame(person) {
      if (person.timestampSelected) {
          this.http.get("http://127.0.0.1:5000/timestamp/"+person.timestampSelected, {responseType: 'text'})
            .subscribe((data) => {
                var newSearch = this.searchService.activeSearch.getValue();
                newSearch.query.frameImage = data;
                this.searchService.activeSearch.next(newSearch);
          });
          this.http.get("http://127.0.0.1:5000/timestamp_detections/"+person.timestampSelected)
            .subscribe((data) => {

                var newSearch = this.searchService.activeSearch.getValue();
                newSearch.target_frame = {
                    people: data
                };
                this.searchService.activeSearch.next(newSearch);
          });
      }
  }

  public restartSearch() {
      var newSearch = this.searchService.activeSearch.getValue();
      newSearch.target_frame = undefined;
      this.searchService.activeSearch.next(newSearch);
  }
}
