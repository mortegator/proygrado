export class Pattern {
    id: number;
    type: string;
    color: string;
    perimeter: any;
}
