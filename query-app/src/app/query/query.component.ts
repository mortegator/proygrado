import { Component, OnInit, ViewChildren} from '@angular/core';
import { VideoCanvasComponent } from '../video-canvas/video-canvas.component';
import { QueryService } from '../query.service';
import { Pattern, Query} from '../pattern';

@Component({
  selector: 'query',
  templateUrl: './query.component.html',
  styleUrls: ['./query.component.css']
})
export class QueryComponent implements OnInit {

  query: Query;

  constructor(public queryService: QueryService) {
  }

  ngOnInit() {
      this.getQuery();
  }

  getQuery(): void {
      this.queryService.query$.subscribe(
          query => this.query = query
      );
  }

  removePattern(pattern): void {
      this.queryService.removePattern(pattern);
  }

  doSearch(): void {
      this.queryService.doSearch();
  }

}
