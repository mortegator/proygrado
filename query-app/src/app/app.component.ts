import { Component, ViewChild, ElementRef} from '@angular/core';
import { QueryService } from './query.service';
import { SearchService } from './search.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Search } from './search';
import {NgbTabChangeEvent} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {


  @ViewChild('tabsetVideo') tabsetVideo: ElementRef;
  activeId = "newSearchTab";
  activeSearch = null;
  private heroesUrl = '';

  title = 'query-app';
  searches = [];
  constructor(
      public queryService: QueryService,
      public searchService: SearchService,
      private http: HttpClient)
      { }

   ngOnInit() {
       this.getSearches();
   }

   getSearches(): void{
       this.searchService.searches$.subscribe( searches => {
           this.searches = searches;
           // this.activeSearch = this.searches.find(s => s.active);
           // if (this.activeSearch) {
           //     this.activeId = 'tab-search'+this.activeSearch.localId;
           // }
       });
   }

   public tabChange($event: NgbTabChangeEvent) {
     if ($event.nextId != 'newSearchTab') {
         var tabName = $event.nextId;
         var searchId = tabName.match(/\d+/)[0];
         var selectedSearch = this.searches.find(s => s.getValue().localId == searchId);
         this.searchService.activeSearch.next(selectedSearch.getValue());
         console.log('cambie active search');
     }
   }



}
