import { Component, OnInit,ViewChild, Injectable, ElementRef, Input } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Search } from '../search';
import { SearchService } from '../search.service';
import { interval } from 'rxjs';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

declare var window: {
  h337: {
    create: Function,
    register: Function,
  }
};

@Component({
  selector: 'show-search',
  templateUrl: './show-search.component.html',
  styleUrls: ['./show-search.component.css']
})
export class ShowSearchComponent implements OnInit {

    public search: Search;
   @ViewChild('canvas') myCanvas: ElementRef;
   @ViewChild('heatmap') myHeatmap: ElementRef;
   public context: CanvasRenderingContext2D;

    base_image : any;
    canvas;
    heatmap;
    ctx;
    heatmapInstance;
    h337;

    constructor(
        private http: HttpClient,
        private searchService: SearchService)
        { }

    ngOnInit() {
        this.canvas = this.myCanvas.nativeElement;
        this.heatmap = this.myHeatmap.nativeElement;
        this.ctx = this.canvas.getContext('2d');
        this.getSearch();
    }

    getSearch() {
        this.searchService.activeSearch$.subscribe(search => {
            this.search = search;
            this.base_image = new Image();
            this.base_image.src = "data:image/png;base64," + search.query.frameImage;
            this.base_image.onload = () => {
                var ctx = this.myCanvas.nativeElement.getContext('2d');
                ctx.drawImage(this.base_image, 0, 0, this.myCanvas.nativeElement.width, this.myCanvas.nativeElement.height);
                var render_average = true;
                // if (search.results) {
                //     for(let person of search.results.people) {
                //         if (person.mark_draw){
                //             this.render_pose(person.pose)
                //             render_average = false;
                //         }
                //     }
                //     if (render_average) {
                //         this.render_pose(search.results.promedio)
                //     }
                // }
                if (search.target_frame) {
                    for(let person of search.target_frame.people) {
                        this.render_pose(person.pose);
                        this.render_bbox(person);
                    }
                } else {
                    if (search.query) {
                        this.drawQuery(search.query);
                    }
                    // heatmap
                    if (search.results.heatmap) {
                        var heatmap_data = search.results.heatmap;
                        var points = [];
                        var max = 0;
                        for(let ix=0; ix<heatmap_data.xedges.length-1; ix++){
                            for(let iy=0; iy<heatmap_data.yedges.length-1; iy++){
                                var val = heatmap_data.heatmap[ix][iy];
                                if (val != 0) {
                                    points.push({
                                        x: heatmap_data.xedges[ix],
                                        y: heatmap_data.yedges[iy],
                                        value: val
                                    });
                                    max = Math.max(max, val);
                                }
                            }
                        }
                      console.log('build heat');
                      this.heatmapInstance = window.h337.create({
                        container: document.getElementById('heatmap')
                      });
                      this.heatmapInstance.setData({
                          max: max,
                          data: points
                      });
                       this.heatmapInstance.repaint();
                   }
                }

            }
        })
    }
    ngAfterViewInit() {
      this.context = (<HTMLCanvasElement>this.myCanvas.nativeElement).getContext('2d');
      this.canvas.style.width ='1280px';
      this.canvas.style.height='738px';
      this.canvas.width = this.canvas.offsetWidth;
      this.canvas.height = this.canvas.offsetHeight;

      this.heatmap.width = '1280px';
      this.heatmap.height = '738px';
      this.heatmap.style.width = this.canvas.offsetWidth + 'px';
      this.heatmap.style.height= this.canvas.offsetHeight + 'px';

    }

    //let tuplas: [number, number][];
    unions = [ [1,8],[1,2],[1,5],[2,3],[3,4],[5,6],[6,7],[8,9],[9,10],[10,11],
    [8,12],[12,13],[13,14],[1,0],[0,15],[15,17],[0,16],[16,18],[14,19],[19,20],
    [14,21],[11,22],[22,23],[11,24] ];

    colors = [ [255,0,85], [255,0,0], [255,85,0], [255,170,0], [255, 255,0],
    [170,255,0], [85,255,0], [0,255,0], [255,0,0], [0,255,85],
    [0,255,170], [0,255,255], [0,170,255], [0,85,255], [0,0,255],
    [255,0,170], [170,0,255], [255,0,255], [85,0,255], [0,0,255], [0,0,255],
    [0,0,255], [0,255,255],[0,255,255] ];

    render_pose(pose) {
        var ctx = this.myCanvas.nativeElement.getContext('2d');
        ctx.lineWidth = 5;
        for (let points of this.unions){
            if (pose[points[0]][0] != 0 && pose[points[0]][1] != 0 && pose[points[1]][0] != 0 && pose[points[1]][1] != 0){
                ctx.beginPath();
                ctx.moveTo(pose[points[0]][0],pose[points[0]][1]);
                ctx.lineTo(pose[points[1]][0],pose[points[1]][1]);
                var color = this.colors[this.unions.indexOf(points)];
                ctx.strokeStyle = 'rgb('+ color[0] + ',' + color[1] + ',' + color[2] + ')';
                ctx.stroke();
                ctx.closePath();
            }
        }
    }

    render_bbox(person) {
        // Obtiene los puntos máximos y minimos
        var pose = person.pose.filter(p=> !(p[0]== 0 && p[1] == 0));
        var xs = pose.map(p => p[0]);
        var ys = pose.map(p => p[1]);
        var x1 = Math.min(...xs);
        var y1 = Math.min(...ys);
        var x2 = Math.max(...xs);
        var y2 = Math.max(...ys);
        // Dibuja caja
        var ctx = this.myCanvas.nativeElement.getContext('2d');
        ctx.lineWidth = 6;
        ctx.beginPath();
        ctx.moveTo(x1,y1);
        ctx.lineTo(x2,y1);
        ctx.lineTo(x2,y2);
        ctx.lineTo(x1,y2);
        ctx.lineTo(x1,y1);
        ctx.strokeStyle = 'rgb(255,255,255)';
        ctx.stroke();
        ctx.closePath();
        // Dibujo texto
        ctx.font="20px Georgia";
        ctx.textBaseline = 'top';
        var text = 'Person #' + person.pid;
        var width = ctx.measureText(text).width;
        // Color background
        ctx.fillStyle = '#ffffff';
        ctx.fillRect(x1-3, y1-20, width, parseInt(ctx.font, 10));
        ctx.fillStyle = '#000';
        ctx.fillText(text,x1-3,y1-20);
    }

    drawQuery(query): void {
        var ctx = this.myCanvas.nativeElement.getContext('2d');
        query.patterns.forEach(pattern=> {
            this.draw(pattern.perimeter,true, pattern.color);
        })
    }

    draw(perimeter, end, fillColor?){
        var ctx = this.ctx;
        ctx.lineWidth = 1;
        ctx.strokeStyle = "white";
        ctx.lineCap = "square";
        ctx.beginPath();

        for(var i=0; i<perimeter.length; i++){
            if(i==0){
                ctx.moveTo(perimeter[i]['x'],perimeter[i]['y']);
                end || this.point(perimeter[i]['x'],perimeter[i]['y']);
            } else {
                ctx.lineTo(perimeter[i]['x'],perimeter[i]['y']);
                end || this.point(perimeter[i]['x'],perimeter[i]['y']);
            }
        }
        if(end){
            ctx.lineTo(perimeter[0]['x'],perimeter[0]['y']);
            ctx.closePath();
            ctx.fillStyle = fillColor;
            ctx.fill();
            ctx.strokeStyle = 'blue';
        }
        ctx.stroke();
    }

    point(x, y){
        this.ctx.fillStyle="white";
        this.ctx.strokeStyle = "white";
        this.ctx.fillRect(x-2,y-2,4,4);
        this.ctx.moveTo(x,y);
    }


}
