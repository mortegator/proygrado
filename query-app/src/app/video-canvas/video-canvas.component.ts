import { Component, OnInit,ViewChild, Injectable, ElementRef } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { QueryService } from '../query.service';
import { h337} from 'heatmap.js';

declare var h337: any;
@Component({
  selector: 'video-canvas',
  templateUrl: './video-canvas.component.html',
  styleUrls: ['./video-canvas.component.css']
})
@Injectable()
export class VideoCanvasComponent implements OnInit {

  @ViewChild('canvas') myCanvas: ElementRef;
  @ViewChild('heatmap') myHeatmap: ElementRef;
  public context: CanvasRenderingContext2D;
  base_image : any;
  canvas;
  heatmap;
  ctx;
  constructor(
      private http: HttpClient,
      public queryService: QueryService )
      { }


  ngOnInit() {
      this.canvas = this.myCanvas.nativeElement;
      this.heatmap = this.myHeatmap.nativeElement;
      this.ctx = this.canvas.getContext('2d');
      this.canvas.addEventListener('mousedown',(event) => this.point_it(event, this));
  }

  buildHeatmap(): void {
      this.http.get("http://127.0.0.1:5000/heatmap")
        .subscribe((data_) => {
            var data = data_ as any;
            console.log(data);
            var points = [];
            var max = 0;
            for(let ix=0; ix<data.xedges.length-1; ix++){
                for(let iy=0; iy<data.yedges.length-1; iy++){
                    var val = data.heatmap[ix][iy];
                    if (val != 0) {
                        points.push({
                            x: data.xedges[ix],
                            y: data.yedges[iy],
                            value: val
                        });
                        max = Math.max(max, val);
                    }
                }
            }
          console.log('build heat');
          var heatmapInstance = h337.create({
            container: document.getElementById('heatmap')
          });
          heatmapInstance.setData({
              max: max,
              data: points
          })
      });

    // // now generate some random data
    // var points = [];
    // var max = 0;
    // var width = this.canvas.offsetWidth;
    // var height = this.canvas.offsetHeight;
    // var len = 200;
    //
    // while (len--) {
    //   var val = Math.floor(Math.random()*100);
    //   max = Math.max(max, val);
    //   var point = {
    //     x: Math.floor(Math.random()*width),
    //     y: Math.floor(Math.random()*height),
    //     value: val
    //   };
    //   points.push(point);
    // }
    // // heatmap data format
    // var data = {
    //   max: max,
    //   data: points
    // };
    // if you have a set of datapoints always use setData instead of addData
    // for data initialization
    // heatmapInstance.setData(data);
  }

  getQuery(): void {
      this.queryService.query$.subscribe(
          query => {
              if (!query.frameImage) {
                  return;
              }
              var base_image = new Image();
              base_image.src = "data:image/png;base64," + query.frameImage;
              var ctx = this.myCanvas.nativeElement.getContext('2d');
              base_image.onload = () => {
                  var ctx = this.myCanvas.nativeElement.getContext('2d');
                  ctx.drawImage(base_image, 0, 0, this.myCanvas.nativeElement.width, this.myCanvas.nativeElement.height);
                  query.patterns.forEach(pattern=> {
                      this.draw(pattern.perimeter,true, pattern.color);
                  })
              }
          }
      );
  }


  ngAfterViewInit() {
    this.context = (<HTMLCanvasElement>this.myCanvas.nativeElement).getContext('2d');

    this.canvas.style.width ='1280px';
    this.canvas.style.height='738px';
    this.canvas.width = this.canvas.offsetWidth;
    this.canvas.height = this.canvas.offsetHeight;

    this.heatmap.width = '1280px';
    this.heatmap.height = '738px';
    this.heatmap.style.width = this.canvas.offsetWidth + 'px';
    this.heatmap.style.height= this.canvas.offsetHeight + 'px';
    this.getQuery();
    this.buildHeatmap();
  }

  finishPolygon(perimeter, color) {
      this.queryService.addPolygon(perimeter, color);
  }





   perimeter = new Array();

  line_intersects(p0, p1, p2, p3) {
      var s1_x, s1_y, s2_x, s2_y;
      s1_x = p1['x'] - p0['x'];
      s1_y = p1['y'] - p0['y'];
      s2_x = p3['x'] - p2['x'];
      s2_y = p3['y'] - p2['y'];

      var s, t;
      s = (-s1_y * (p0['x'] - p2['x']) + s1_x * (p0['y'] - p2['y'])) / (-s2_x * s1_y + s1_x * s2_y);
      t = ( s2_x * (p0['y'] - p2['y']) - s2_y * (p0['x'] - p2['x'])) / (-s2_x * s1_y + s1_x * s2_y);

      if (s >= 0 && s <= 1 && t >= 0 && t <= 1)
      {
          // Collision detected
          return true;
      }
      return false; // No collision
  }

  point(x, y){
      this.ctx.fillStyle="white";
      this.ctx.strokeStyle = "white";
      this.ctx.fillRect(x-2,y-2,4,4);
      this.ctx.moveTo(x,y);
  }

  undo(){
      this.perimeter.pop();
      this.start(true);
  }

  clear_canvas(){
      this.perimeter = new Array();
      this.start(false);
  }

  draw(perimeter, end, fillColor?){
      var ctx = this.ctx;
      ctx.lineWidth = 1;
      ctx.strokeStyle = "white";
      ctx.lineCap = "square";
      ctx.beginPath();

      for(var i=0; i<perimeter.length; i++){
          if(i==0){
              ctx.moveTo(perimeter[i]['x'],perimeter[i]['y']);
              end || this.point(perimeter[i]['x'],perimeter[i]['y']);
          } else {
              ctx.lineTo(perimeter[i]['x'],perimeter[i]['y']);
              end || this.point(perimeter[i]['x'],perimeter[i]['y']);
          }
      }
      if(end){
          ctx.lineTo(perimeter[0]['x'],perimeter[0]['y']);
          ctx.closePath();
          ctx.fillStyle = fillColor;
          ctx.fill();
          ctx.strokeStyle = 'blue';
      }
      ctx.stroke();
  }

  check_intersect(x,y){
      if(this.perimeter.length < 4){
          return false;
      }
      var p0 = new Array();
      var p1 = new Array();
      var p2 = new Array();
      var p3 = new Array();

      p2['x'] = this.perimeter[this.perimeter.length-1]['x'];
      p2['y'] = this.perimeter[this.perimeter.length-1]['y'];
      p3['x'] = x;
      p3['y'] = y;

      for(var i=0; i<this.perimeter.length-1; i++){
          p0['x'] = this.perimeter[i]['x'];
          p0['y'] = this.perimeter[i]['y'];
          p1['x'] = this.perimeter[i+1]['x'];
          p1['y'] = this.perimeter[i+1]['y'];
          if(p1['x'] == p2['x'] && p1['y'] == p2['y']){ continue; }
          if(p0['x'] == p3['x'] && p0['y'] == p3['y']){ continue; }
          if(this.line_intersects(p0,p1,p2,p3)==true){
              return true;
          }
      }
      return false;
  }

  point_it(event, service) {
      var canvas = this.canvas;
      var ctx = this.ctx;

      var rect, x, y;

      rect = canvas.getBoundingClientRect();
      x = event.clientX - rect.left;
      y = event.clientY - rect.top;
      // Si hace click cerca del primer punto se considera que está cerrando el polígono
      var UMBRAL = 20;
      if (this.perimeter.length > 0 &&  (Math.abs(this.perimeter[0]['x']-x) < UMBRAL) && (Math.abs(this.perimeter[0]['y']-y) < UMBRAL)) {
          if(this.perimeter.length==2){
              alert('You need at least three points for a polygon');
              return false;
          }
          x = this.perimeter[0]['x'];
          y = this.perimeter[0]['y'];
          if(this.check_intersect(x,y)){
              alert('The line you are drowing intersect another line');
              return false;
          }
          this.draw(this.perimeter,true,  this.queryService.getNewColor());
          this.finishPolygon(this.perimeter, ctx.fillStyle);
          this.perimeter = [];
          event.preventDefault();
          return false;
      }
      if (this.perimeter.length>0 && x == this.perimeter[this.perimeter.length-1]['x'] && y == this.perimeter[this.perimeter.length-1]['y']){
          // same point - double click
          return false;
      }
      if(this.check_intersect(x,y)){
          alert('The line you are drowing intersect another line');
          return false;
      }
      this.perimeter.push({'x':x,'y':y});
      this.draw(this.perimeter,false);
      return false;

  }

  start(with_draw) {
      var canvas = this.canvas;
      var ctx = this.ctx;

      var img = new Image();
      img.src = canvas.getAttribute('data-imgsrc');

      img.onload = () => {
          ctx = canvas.getContext("2d");
          ctx.drawImage(img, 0, 0, canvas.width, canvas.height);
          if(with_draw == true){
              this.draw(this.perimeter,false);
          }
      }
  }


}
