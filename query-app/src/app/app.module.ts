import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { AppComponent } from './app.component';
import { VideoCanvasComponent } from './video-canvas/video-canvas.component';
import { HttpClientModule } from '@angular/common/http';
import { QueryComponent } from './query/query.component';
import { QueryService} from './query.service';;
import { SearchService} from './search.service';
import { ShowSearchComponent } from './show-search/show-search.component';
import { ResultListComponent } from './result-list/result-list.component';
import { NgCircleProgressModule } from 'ng-circle-progress';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatSliderModule} from '@angular/material/slider';
import { FormsModule } from '@angular/forms';
import { HighchartsChartModule } from 'highcharts-angular';
import { Ng5SliderModule } from 'ng5-slider';
import { FilterIntervalPipe} from './filterInterval.pipe';

@NgModule({
  declarations: [
    AppComponent,
    VideoCanvasComponent,
    QueryComponent,
    ShowSearchComponent,
    ResultListComponent,
    FilterIntervalPipe
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    NgbModule,
    BrowserAnimationsModule,
    MatSliderModule,
    FormsModule,
    HighchartsChartModule,
    Ng5SliderModule,
    NgCircleProgressModule.forRoot({
     "backgroundColor": "#DDDDDD",
     "backgroundPadding": -10,
     "radius": 60,
     "maxPercent": 100,
     "outerStrokeWidth": 10,
     "outerStrokeColor": "#61A9DC",
     "innerStrokeWidth": 0,
     "subtitleColor": "#444444",
     "showInnerStroke": false,
     "startFromZero": false
    })
  ],
  providers: [QueryService, SearchService],
  bootstrap: [AppComponent]
})
export class AppModule { }
