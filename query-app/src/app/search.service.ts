import { Injectable, Output, EventEmitter } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Search} from './search';
import { Pattern, Query } from './pattern';
import { v4 as uuid } from 'uuid';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class SearchService {

  public searches = new BehaviorSubject<BehaviorSubject<Search>[]>([]);
  public searches$ = this.searches.asObservable();

  public activeSearch = new BehaviorSubject<Search>(new Search());
  public activeSearch$ = this.activeSearch.asObservable();

  constructor(
      private http: HttpClient
  ) { }

  newSearch(query: Query) {
      var new_search = new Search();
      new_search.localId = this.searches.getValue().length + 1;
      new_search.active = true;
      new_search.loading = true;
      new_search.query = query;

      var searchObservable = new BehaviorSubject<Search>(new_search);
      this.searches.getValue().push(searchObservable);
      this.searches.next(this.searches.getValue());
      var query_points = query.patterns[0].perimeter;
      this.http.post("http://127.0.0.1:5000/query/start", query_points)
            .subscribe((response) => {
                var data = response as any;
                new_search.serverId = data.thread;
                this.updateProgress(searchObservable);
      });
  }

  updateProgress(searchObservable: BehaviorSubject<Search>) {
      var search = searchObservable.getValue();
      this.http.get("http://127.0.0.1:5000/query/progress/" + search.serverId)
        .subscribe((response) => {
            var data = response as any;
            search.progress = Math.round(data.progress * 100);
            search.loading = ! data.finished;
            searchObservable.next(search);
            if (search.loading) {
                setTimeout(() => {
                    this.updateProgress(searchObservable);
                }, 3*1000);
            } else {
                this.getResults(searchObservable);
            }
        })
  }

  getResults(searchObservable:  BehaviorSubject<Search>) {
      var search = searchObservable.getValue();
      this.http.get("http://127.0.0.1:5000/query/result/" + search.serverId)
        .subscribe((response) => {
            var data = response as any;
            search.results = data;
            search.results.people.forEach(p => p.duracion = Math.round((p.timestamp_end - p.timestamp_start)/60) );
            searchObservable.next(search);
        })
  }
}
