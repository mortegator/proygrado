import { Pipe, PipeTransform } from '@angular/core';
@Pipe({
  name: 'filterInterval'
})
export class FilterIntervalPipe implements PipeTransform {
  transform(items: any[], minFilterValue: number , maxFilterValue : number, minFilterTimestampValue: number, maxFilterTimestampValue : number): any[] {
    if(!items) return [];
    return items.filter( p => {
        return p.duracion >= minFilterValue && p.duracion <= maxFilterValue &&
            p.timestamp_start >= minFilterTimestampValue && p.timestamp_start <= maxFilterTimestampValue
    });
   }
}
