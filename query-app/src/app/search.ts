import { Pattern, Query } from './pattern';
import { EventEmitter } from '@angular/core';

export class Search {
    localId: number;
    serverId: string;
    active: boolean;
    query: Query;
    newSearchEvent = new EventEmitter<String>();
    loading: boolean;
    results: any;
    progress: number;
    target_frame: any;
}

export class SearchResults {
    people: PersonResult[];
    averagePose: any[];
}

export class PersonResult {
    pid: string;
    frames: number;
    pose: any[];
}
