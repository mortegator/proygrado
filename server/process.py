# -*- coding: utf-8 -*-
import configparser
import os
import sys
import cv2
import time
import numpy as np
from utils import poses2boxes
from pymongo import MongoClient
import json
import datetime
import dateutil

from deep_sort.iou_matching import iou_cost
from deep_sort.kalman_filter import KalmanFilter
from deep_sort.detection import Detection
from deep_sort.tracker import Tracker as DeepTracker
from deep_sort import nn_matching
from deep_sort import preprocessing
from deep_sort.linear_assignment import min_cost_matching
from deep_sort.detection import Detection as ddet
from tools import generate_detections as gdet

COLOURS = np.random.rand(32, 3) * 255
frame_start = 0

max_cosine_distance = 0.3
nn_budget = None
nms_max_overlap = 1.0
max_age = 20
n_init = 20

TIMESTAMP_BEGIN = datetime.datetime(1970, 1, 1, 0, 0, tzinfo=dateutil.tz.tzoffset(None, 0))

def get_video_dimension(video_path):
    cap = cv2.VideoCapture(video_path)
    if cap.isOpened():
        width = cap.get(cv2.CAP_PROP_FRAME_WIDTH)
        height = cap.get(cv2.CAP_PROP_FRAME_HEIGHT)
        cap.release()
        return (width, height)
    else:
        return (0,0)

boxes = None
keypoints = None
frame = None
def get_video_time(video_path):
    _ , video_name = os.path.split(video_path)
    str_cam, str_time = video_name.split('.')[0].split('_')
    cap = cv2.VideoCapture(video_path)
    frames_length = int(cap.get(cv2.CAP_PROP_FRAME_COUNT))
    cap.release()

    video_datetime_start = dateutil.parser.parse(str_time + "-0300")
    # Timestamp del comienzo del video, en GMT timezone
    video_timestamp_start = (video_datetime_start - TIMESTAMP_BEGIN).total_seconds()
    # Timestamp del fin del video
    video_timestamp_end = (frames_length / 25) + video_timestamp_start
    return video_timestamp_start, video_timestamp_end

def process_video(video_path, detections_folder,output_path):
    global keypoints
    global boxes
    global detections
    global frame
    global track
    global people
    global person
    global poses
    people = {}
    _ , video_name = os.path.split(video_path)
    video_name = video_name.split('.')[0]

    cap = cv2.VideoCapture(video_path)
    cap.set(cv2.CAP_PROP_POS_FRAMES, frame_start)
    # Define the codec and create VideoWriter object
    fourcc = cv2.VideoWriter_fourcc(*'XVID')
    #  TODO: Resolucion de video igual a al a de input!!!!
    out = cv2.VideoWriter(output_path,fourcc, 30.0, (1280,720))

    seen_ids = []
    frame_i = frame_start
    frame_i -= 1 # Porque se hace el ++ antes del procesamiento
    while(cap.isOpened()):
        frame_i += 1
        ret, frame = cap.read()
        if frame_i % 5 != 0:
            continue
        if ret==True:
            # Get keypoints from OpenPose
            #keypoints, frame = openpose.forward(frame, True)
            raw_filename = "%s/%s_%012d_keypoints.json"%(detections_folder,video_name,frame_i)
            with open(raw_filename) as raw_file:
                raw_detections = json.load(raw_file)
                keypoints = np.zeros((len(raw_detections['people']), 25, 3))
                for i, person in enumerate(raw_detections['people']):
                    keypoints_person = person['pose_keypoints_2d']
                    kp = np.reshape(keypoints_person, (25,3)).astype(int)
                    for x,y,_ in kp:
                        cv2.circle(frame, (int(x),int(y)), 3, (255,0,0), thickness = -1)
                    keypoints[i,:,:] = kp

            # No uso el XYConfidence
            poses = keypoints[:,:,:2]
            # Get containing box for each seen body
            boxes = poses2boxes(poses)
            #Filtro por area
            P1 = (246,149)
            P2 = (1000,266)
            inside_area = lambda p: np.cross(np.array(P1)-p, np.array(P2)-p) > 0
            cv2.line(frame, P1, P2, (255,0,0), thickness = 3)
            filtered_boxes_and_poses = [ [[x1,y1,x2,y2],pose] for [x1,y1,x2,y2],pose in zip(boxes,poses) if inside_area([x2,y2])]
            boxes = [x[0] for x in filtered_boxes_and_poses]
            poses = [x[1] for x in filtered_boxes_and_poses]

            #Dibuja cajas de poses vistas
            for bbox in boxes:
                cv2.rectangle(frame, (int(bbox[0]), int(bbox[1])), (int(bbox[2]), int(bbox[3])),(0,123,123), 6)


            boxes_xywh = [[x1,y1,x2-x1,y2-y1] for [x1,y1,x2,y2] in boxes]
            features = encoder(frame,boxes_xywh)
            # score to 1.0 here).
            nonempty = lambda xywh: xywh[2] != 0 and xywh[3] != 0
            detections = [Detection(bbox, 1.0, feature, pose) for bbox, feature, pose in zip(boxes_xywh, features, poses) if nonempty(bbox)]
            # Run non-maxima suppression.
            boxes_det = np.array([d.tlwh for d in detections])
            scores = np.array([d.confidence for d in detections])
            indices = preprocessing.non_max_suppression(boxes_det, nms_max_overlap, scores)
            detections = [detections[i] for i in indices]
            # Call the tracker
            tracker.predict()
            tracker.update(frame, detections)

            for track in tracker.tracks:
                # Agrego a arreglo de personas el track si fue recién visto, si la detección es de este frame
                if track.track_id not in people:
                    people[track.track_id] = {}
                people[track.track_id][frame_i] = track.last_seen_detection.pose

                color = None
                if not track.is_confirmed():
                    color = (0,0,255)
                else:
                    color = (255,255,255)
                bbox = track.to_tlbr()
                cv2.rectangle(frame, (int(bbox[0]), int(bbox[1])), (int(bbox[2]), int(bbox[3])),color, 2)
                cv2.putText(frame, "id%s - ts%s"%(track.track_id,track.time_since_update),(int(bbox[0]), int(bbox[1])-20),0, 5e-3 * 200, (0,255,0),2)

            cv2.putText(frame, str(frame_i),(40,40),0, 5e-3 * 200, (255,255,0),2)
            out.write(frame)
            cv2.imshow("output", frame)
            if cv2.waitKey(1) & 0xFF == ord('q'):
                break
        else:
            print("break")
            break
    cap.release()
    out.release()
    return people

# Cargar configuracion
config = configparser.ConfigParser()
config.read('config.ini')

# Cargar OpenPose:
sys.path.append('/usr/local/python')
#from openpose import *
params = dict()
params["logging_level"] = 3
params["output_resolution"] = "-1x-1"
params["net_resolution"] = "-1x736"
params["model_pose"] = "BODY_25"
params["alpha_pose"] = 0.6
params["scale_gap"] = 0.3
params["scale_number"] = 1
params["render_threshold"] = 0.05
params["num_gpu_start"] = 0
params["disable_blending"] = False
# Ensure you point to the correct path where models are located
params["default_model_folder"] = config['paths']['openpose'] + "/models/"
# openpose = OpenPose(params)


model_filename = 'model_data/mars-small128.pb'
encoder = gdet.create_box_encoder(model_filename,batch_size=1)
metric = nn_matching.NearestNeighborDistanceMetric("cosine", max_cosine_distance, nn_budget)
tracker = DeepTracker(metric, max_age = max_age,n_init= n_init)

mongodb_uri = config['database']['uri']
client = MongoClient(mongodb_uri)
db = client[config['database']['database']]

#Busca videos que no hayan sido procesados aún
videos_collection = db[config['database']['videos_collection']]
people_collection = db[config['database']['people_collection']]
detections_collection = db[config['database']['detections_collection']]
counters_collection =  db[config['database']['counters_collection']]

# Si no existe el counter lo inicializo
if not list(counters_collection.find()):
    counters_collection.insert({
        '_id':"people",
        'sequence_value':0
    })

videos_path = config['paths']['videos']
pose_detections_path = config['paths']['posedetections']
person = None
poses  = None
for filename in sorted(os.listdir(videos_path)):
    if filename.endswith(".mp4"):
        video_doc = videos_collection.find_one({'filename':filename})
        video_path = videos_path+'/'+filename
        if video_doc is not None:
            print("Video {} already processed on {}".format(video_doc['filename'], video_doc['date']))
        else:
            print("{} - Processing".format(filename))
            video_ts_start, video_ts_end = get_video_time(video_path)

            video_traces = process_video(video_path, pose_detections_path ,"./analyzed/"+filename)
            for pid, person in video_traces.items():
                if len(person) > int(config['detection']['min_frames']):
                    # Obtiene counter de id persona
                    current_pid = counters_collection.find_and_modify(
                        {'_id':'people'},
                        {'$inc':{'sequence_value':1}}
                    )['sequence_value']
                    # result = people_collection.insert_one({
                    #     'trace': {str(frame): pose.tolist() for frame, pose in person.items()},
                    #     'video': filename
                    # })
                    for frame, pose in person.items():
                        detections_collection.insert_one({
                            'pid': current_pid,
                            'video': filename,
                            'frame': frame,
                            'pose': pose.tolist(),
                            'timestamp': video_ts_start + (frame / 25)
                        })

            print("{} - Processed".format(filename))
            videos_collection.insert_one({
                'filename': filename,
                'date': time.strftime("%d/%m/%Y %H:%M:%S"),
                'dimension': get_video_dimension(video_path),
                'timestamp_start': video_ts_start,
                'timestamp_end': video_ts_end
            })
