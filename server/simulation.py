# -*- coding: utf-8 -*-
import configparser
import os
import sys
import cv2
import time
import numpy as np
from utils import poses2boxes
from pymongo import MongoClient
import json


from deep_sort.iou_matching import iou_cost
from deep_sort.kalman_filter import KalmanFilter
from deep_sort.detection import Detection
from deep_sort.tracker import Tracker as DeepTracker
from deep_sort import nn_matching
from deep_sort import preprocessing
from deep_sort.linear_assignment import min_cost_matching
from deep_sort.detection import Detection as ddet
from tools import generate_detections as gdet
from deep_sort.my_filter import MyKalmanFilter
from deep_sort.track import Track


def move(box):
    tlx, tly, w,h = box
    return [tlx,tly+100,w,h]


SHIFT_X = 300
box = [50,50,100,100]
box2 = [50+SHIFT_X,50,100,100]


measurement = Detection(box, 1.0, np.zeros((128)))
measurement2 = Detection(box2, 1.0, np.zeros((128)))

# TRACKER UNO
ndim, dt = 4, 1.
motion_mat = np.eye(2 * ndim, 2 * ndim)
for i in range(ndim):
    motion_mat[i, ndim + i] = dt
observation_mat = np.eye(ndim, 2 * ndim)
tracker = MyKalmanFilter(motion_mat, observation_mat)
mean, covariance = tracker.initiate(measurement.to_xyah())

# TRACKER DOS
ndim, dt = 4, 1.
motion_mat = np.eye(2 * ndim, 2 * ndim)
for i in range(ndim):
    motion_mat[i, ndim + i] = dt
observation_mat = np.eye(ndim, 2 * ndim)
tcov = np.eye(2 *ndim, 2 *ndim) * 10
tracker2 = MyKalmanFilter(motion_mat, observation_mat, tcov)
mean2, covariance2 = tracker2.initiate(measurement2.to_xyah())

track = Track(mean, covariance, 1, 1,23,measurement.feature)
track2 = Track(mean2, covariance2, 1, 1,23,measurement.feature)
frame_i = 0
while True:
    frame = np.zeros(((720, 1280, 3)))

    color = (255,255,255)
    bbox = track.to_tlbr()
    cv2.rectangle(frame, (int(bbox[0]), int(bbox[1])), (int(bbox[2]), int(bbox[3])),color, 2)

    color = (255,0,0)
    bbox = track2.to_tlbr()
    cv2.rectangle(frame, (int(bbox[0]), int(bbox[1])), (int(bbox[2]), int(bbox[3])),color, 2)

    if frame_i == 0:
        track.update(tracker, Detection(move(box), 1.0, np.zeros((128))))
        track2.update(tracker2, Detection(move(box2), 1.0, np.zeros((128))))

    track.predict(tracker)
    track2.predict(tracker)

    cv2.putText(frame, str(frame_i),(40,40),0, 5e-3 * 200, (255,255,0),2)
    cv2.imshow("output", frame)
    frame_i += 1
    if cv2.waitKey(0) & 0xFF == ord('q'):
        break
