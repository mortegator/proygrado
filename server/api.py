# -*- coding: utf-8 -*-

from flask import Flask, request, jsonify
from flask import send_file
app = Flask(__name__)

# Es /1 porque potencialmente podria haber mas de una camara y de una escena vacía que mostrar
@app.route("/scene/1")
def empty_scene():
    return send_file("emptyframe.png", mimetype='image/png')


# Recibe un json con la busqueda, imprime la busqueda en pantalla y muestra un resultado cualquiera
# El post tiene que tener el header Content-Type: application/json
# Devuelve las ids de las personas resultado de la busqueda
@app.route("/query_path", methods = ['POST'])
def query_path():
    query = request.get_json(silent=True)
    # Muestra el json recibido
    print query
    return jsonify([14,21,32])

@app.route("/trace/<pid>")
def print_trace(pid):
    if int(pid) in [14,21,32]:
        return send_file("ppl" + pid + ".png", mimetype='image/png')
    else:
        return "No existente"


if __name__ == '__main__':
    app.run(debug=True)
