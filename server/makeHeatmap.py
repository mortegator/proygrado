# -*- coding: utf-8 -*-
from __future__ import print_function
from pymongo import MongoClient
import configparser
from bson.objectid import ObjectId
from utils import iou, poses2boxes, pose2midpoint, distancia_midpoints
import cv2
import numpy as np
import random
from dtw import dtw
import operator
import base64
from matplotlib import pyplot as plt
import shapely
from shapely.geometry import Polygon
import itertools
import json

# Cargar configuracion
config = configparser.ConfigParser()
config.read('config.ini')
mongodb_uri = config['database']['uri']
client = MongoClient(mongodb_uri)
db = client[config['database']['database']]

detections_collection = db[config['database']['detections_collection']]
all_detections = list(detections_collection.find())

points_x, points_y = [], []
i = 0
for i, d in enumerate(all_detections):
    x, y = pose2midpoint(np.array(d['pose']))
    points_x.append(x)
    points_y.append(y)
heatmap, xedges, yedges = np.histogram2d(points_x,points_y, bins= (80,62), range= [[0,1280],[0,738]])

heatmap_collection = db[config['database']['heatmap_collection']]
heatmap_collection.delete_many({})
heatmap_collection.insert_one({
    'heatmap': heatmap.tolist(),
    'xedges': xedges.tolist(),
    'yedges': yedges.tolist()
})
