# -*- coding: utf-8 -*-
from __future__ import print_function
from flask import Flask, request, jsonify, make_response
from flask import send_file
from pymongo import MongoClient
import configparser
from bson.objectid import ObjectId
from utils import iou, poses2boxes, pose2midpoint, distancia_midpoints
import cv2
import numpy as np
import random
from dtw import dtw
import operator
from flask_cors import CORS, cross_origin
import base64
#from raycasting import Polygon, Point, PointInPolygon
from matplotlib import pyplot as plt
import shapely
from shapely.geometry import Polygon
import itertools
import json
import uuid
import threading

CACHE = False

app = Flask(__name__)

# Cargar configuracion
config = configparser.ConfigParser()
config.read('config.ini')
mongodb_uri = config['database']['uri']
client = MongoClient(mongodb_uri)
db = client[config['database']['database']]

detections_collection = db[config['database']['detections_collection']]

print("Getting all detections")
# all_detections = list(detections_collection.find())
print("--> Finished")
# Comienza quedandose con todas las trazas en memoria para hacer el query_path mas rapido
# midpoint_traces = {}
# people_collection = db[config['database']['people_collection']]
# cursor = people_collection.find({})
# for person in cursor:
#     person_midtrace = {}
#     frames_ordered = sorted([int(f) for f in person['trace'].keys()])
#     midpoint_traces[str(person['_id'])] = [pose2midpoint(np.array(person['trace'][str(frame)])) for frame in frames_ordered]

class QueryThread(threading.Thread):
    def __init__(self, query_points):
        super(QueryThread, self).__init__()
        self.query_points = query_points
        self.progress = 0

    def run(self):
        global all_detections
        query_points = self.query_points
        query_poli = Polygon(query_points)

        filtered_detections = []

        i = 0
        cursor_all = detections_collection.find().limit(10*1000)
        total_i = cursor_all.count()
        for detection in cursor_all:
            #Guarda progreso de este for que e el que mas tiempo lleva
            self.progress = float(i)/total_i
            pose = np.reshape(detection['pose'], (25,2)).astype(int)
            bbox = poses2boxes([pose])[0]
            bbox_polygon = Polygon([(bbox[0],bbox[1]),(bbox[2],bbox[1]),(bbox[2],bbox[3]),(bbox[0],bbox[3])])
            i+=1
            if bbox_polygon.area == 0:
                continue
            intersection_area = query_poli.intersection(bbox_polygon).area
            if (intersection_area / bbox_polygon.area > 0.7):
                filtered_detections.append(detection)


        MAX_GAP_ALLOWED = 1500
        MIN_DETECTIONS_REQUIRED = 300

        get_pid = operator.itemgetter('pid')
        detections_bypid = [list(g) for k, g in itertools.groupby(sorted(filtered_detections, key=get_pid), get_pid)]
        #print("Pids vistas: %s"%[e[0]['pid'] for e in detections_bypid ])

        people_poses = []
        people_inarea = []
        for detections_person in detections_bypid:
            pid = detections_person[0]['pid']
            #print("pid %s video %s"%(pid,detections_person[0]['video']))
            detections_sortbyframe = sorted(detections_person, key = lambda det: det['frame'])
            detections_inarea = []
            rachas_area = []
            for i in range(len(detections_sortbyframe)-1):
                d1, d2 = detections_sortbyframe[i], detections_sortbyframe[i+1]
                t1, t2 = d1['frame'], d2['frame']
                if t2-t1 < MAX_GAP_ALLOWED:
                    detections_inarea.append(detections_sortbyframe[i])
                else:
                    # Si se corta la racha de permanencia, entonces muevo la serie de detecciones seguidas
                    # a la lista de resultados y empiezo de cero de vuelta
                    if len(detections_inarea) > MIN_DETECTIONS_REQUIRED:
                        rachas_area.append(detections_inarea)
                    detections_inarea = []
            rachas_area.append(detections_inarea)
            for racha in rachas_area:
                #print("can dets de %s en pid %s"%(len(detections_inarea),pid))
                if len(racha) > MIN_DETECTIONS_REQUIRED:
                    people_inarea.append(racha)
                    break

        # se usan para el heatmap
        points_x, points_y = [], []
        #calculo deteccion promedio para cada persona
        for person in people_inarea:
            poses_array = []
            for det in person:
                poses_array.append(det['pose'])
                x, y = pose2midpoint(np.array(det['pose']))
                points_x.append(x)
                points_y.append(y)

            poses_array = np.array(poses_array)
            avg_det = np.ma.masked_equal(poses_array,[0,0])
            avg_det = avg_det.mean(axis=0).data
            people_poses.append({
                'pid':det['pid'],
                'pose':avg_det.tolist(),
                'frames':len(person),
                'timestamp_start':min([d['timestamp'] for d in person]),
                'timestamp_end':max([d['timestamp'] for d in person]),
                # No se deberian de usar desde el frontend.
                'frame_start':min([d['frame'] for d in person]),
                'frame_end':max([d['frame'] for d in person])
            })
        # Calcular psoe promedio
        if len(people_poses) > 0:
            poses_array = []
            for pose in people_poses:
                poses_array.append(pose['pose'])
            poses_array = np.array(poses_array)
            pose_promedio = np.ma.masked_equal(poses_array,[0,0])
            pose_promedio = pose_promedio.mean(axis=0).data.tolist()
        else:
            pose_promedio = None
        # heatmap
        heatmap, xedges, yedges = np.histogram2d(points_x,points_y, bins= (80,62), range= [[0,1280],[0,738]])
        self.result = {
            'promedio': pose_promedio,
            'people': people_poses,
            'heatmap': {
                'heatmap': heatmap.tolist(),
                'xedges': xedges.tolist(),
                'yedges': yedges.tolist()
            }
        }

def random_pid():
    people_collection = db[config['database']['people_collection']]
    count = people_collection.count()
    random_pid = people_collection.find()[random.randrange(count)]['_id']
    return random_pid

@app.route("/heatmap")
def heatmap():
    heatmap_collection = db[config['database']['heatmap_collection']]
    heatmap = heatmap_collection.find_one({},{'_id': False})
    return jsonify(heatmap)

def show(xedges,yedges,heatmap):
    extent = [xedges[0], xedges[-1], yedges[0], yedges[-1]]
    # Plot heatmap
    plt.clf()
    plt.title('Pythonspot.com heatmap example')
    plt.ylabel('y')
    plt.xlabel('x')
    plt.imshow(heatmap, extent=extent)
    plt.show()

# Es /1 porque potencialmente podria haber mas de una camara y de una escena vacía que mostrar
@app.route("/scene/1")
def empty_scene():
    return base64.b64encode(open("numeracion-mesas.png", "rb").read())

# Da informacion inicial
@app.route("/init_info")
def init_info():
    min_timestamp = detections_collection.find_one(sort=[("timestamp",1)])["timestamp"]
    max_timestamp = detections_collection.find_one(sort=[("timestamp",-1)])["timestamp"]
    return jsonify({
        "max_ts": max_timestamp,
        "min_ts": min_timestamp
    })

def timestamp_to_video(timestamp):
    videos_collection = db[config['database']['videos_collection']]
    videos = list(videos_collection.find())
    videos = filter(lambda v: v['timestamp_start'] <= timestamp and v['timestamp_end'] >= timestamp, videos)
    if len(videos) == 0:
        return "No se encuentra video para {}".format(timestamp)
    video = videos[0]
    target_frame = (timestamp - video['timestamp_start']) * 25
    video_path = config['paths']['videos'] + '/' + video['filename']
    return video, video_path, target_frame

@app.route("/timestamp_detections/<timestamp>")
def get_frame_data(timestamp):
    timestamp = int(timestamp)
    video, video_path, target_frame = timestamp_to_video(timestamp)
    frame_from, frame_to = target_frame - 5, target_frame + 5
    detections = detections_collection.find({
        "frame" : {
            "$lt" : frame_to,
            "$gt" : frame_from
        },
        "video": video['filename']
    }, {"_id":False })
    return jsonify(list(detections))

@app.route("/timestamp/<timestamp>")
def get_frame(timestamp):
    timestamp = int(timestamp)
    _, video_path, target_frame = timestamp_to_video(timestamp)
    cap = cv2.VideoCapture(video_path)
    print(target_frame)
    cap.set(cv2.CAP_PROP_POS_FRAMES, int(target_frame))
    if cap.isOpened():
        ret, frame = cap.read()
        if ret:
            retval, buffer = cv2.imencode('.jpg', frame)
            img_as_text = base64.b64encode(buffer)
            cap.release()
            return img_as_text
    cap.release()
    return ""

# Recibe un json con la busqueda, imprime la busqueda en pantalla y muestra un resultado cualquiera
# El post tiene que tener el header Content-Type: application/json
# Devuelve las ids de las personas resultado de la busqueda
@app.route("/query_path", methods = ['POST'])
def query_path():
    query = request.get_json(silent=True)
    # Test para forzar la busqueda de la trayectoria de un pid dado
    #query = midpoint_traces['5b370dd06a57590cfa7f7dc3']

    # Calcula la distancia de la query con todas las trazas de puntos medios existentes
    distancias = {}
    for person_id, midpoint_trace in midpoint_traces.items():
        dist, cost, acc, path = dtw(query, midpoint_trace, dist=distancia_midpoints)
        distancias[person_id] = dist
    # Ordeno por distancia
    sorted_x = sorted(distancias.items(), key=operator.itemgetter(1))
    # Retorno las seis mas parecida
    # Tambien se podría retornar las que dist < UMBRAL
    return jsonify([[id, dist] for id, dist in sorted_x[:6]])

processing_threads = {}
@app.route("/query/start", methods = ['POST'])
def start_query_area():
    # Convierto de json al tipo de dato que se usa
    query = request.get_json(silent=True)
    print(query)
    query_points = []
    for point in query:
        query_points.append((point['x'],point['y']))

    # Empiezo el query en un thread nuevo con una id única y devuelvo esa id
    thread_id = str(uuid.uuid4())
    processing_threads[thread_id] = QueryThread(query_points)
    processing_threads[thread_id].start()
    return jsonify({
        'thread': thread_id
    })

# Obtiene el progrso de un query, un numero del 0 al 1
@app.route('/query/progress/<thread_id>')
def get_progress(thread_id):
    global processing_threads
    cache_collection = db['cache']
    cache = cache_collection.find_one()
    if not CACHE or not cache:
        return jsonify({
            'progress': processing_threads[thread_id].progress,
            'finished': not processing_threads[thread_id].is_alive()
        })
    else:
        return jsonify({
            'progress': 100,
            'finished': True
        })

# Obtiene el progrso de un query, un numero del 0 al 1
@app.route('/query/result/<thread_id>')
def get_query_result(thread_id):
    global processing_threads
    cache_collection = db['cache']
    cache = cache_collection.find_one({}, {"_id":False})
    if not CACHE or not cache:
        if (not processing_threads[thread_id].is_alive()):
            if CACHE:
                cache_collection.insert_one(processing_threads[thread_id].result)
            return jsonify(processing_threads[thread_id].result)
        else:
            return None
    else:
        return jsonify(cache)

@app.route("/midpoint_trace/<pid>")
def get_midpoint_trace(pid):
    if pid in midpoint_traces:
        person = people_collection.find_one({
            "_id": ObjectId(pid)
        })
        trace = []
        for frame, pose in person['trace'].items():
            trace.append({
                'frame': frame,
                'midpoint': pose2midpoint(np.array(pose)).tolist()
            })
        return jsonify({
            'pid': pid,
            'trace': trace
        })
    else:
        return "No existente"

# Devuelve una trace al azar de todos los existentes en la base
@app.route("/random_trace")
def random_trace():
    return print_trace(random_pid())


@app.route("/trace")
def print_trace():
    pids = request.args.getlist('pid', type=None)
    people_collection = db[config['database']['people_collection']]
    videos_collection = db[config['database']['videos_collection']]
    output_img = cv2.imread('emptyframe.png',cv2.IMREAD_COLOR)
    for pid in pids:
        #pid  = random_pid()
        person = people_collection.find_one({
            "_id": ObjectId(pid)
        })
        if person is None:
            return "No existente"
        else:
            # Si hay mas de un video para la misma persona habría que cambiar el find_one por uno que encuentre todos
            video_doc = videos_collection.find_one({
                'filename': person['video']
            })
            video_path = "./videos/"+video_doc['filename']
            cap = cv2.VideoCapture(video_path)
            # Para reducir tiempo solo tiene en cuenta 6 frames equidistantes de los que se vio la persona
            frames_ordered = sorted([int(f) for f in person['trace'].keys()])
            for frame_i in range(0,len(frames_ordered), len(frames_ordered)/6):
                # Busco la pose para ese frame
                frame = frames_ordered[frame_i]
                pose = person['trace'][str(frame)]
                box =  poses2boxes(np.array([pose]))[0]

                # Copio la box de la pose a la imagen que voy a devolver
                cap.set(cv2.CAP_PROP_POS_FRAMES, frame)
                ret, frame = cap.read()
                if ret == True:
                    # Agrego la box de ese frame con transparecia alpha a la que voy a devolver
                    overlay = output_img.copy()
                    overlay[box[1]:box[3],box[0]:box[2],:] = frame[box[1]:box[3],box[0]:box[2],:]
                    ALPHA = 0.4
                    cv2.addWeighted(overlay, ALPHA, output_img, 1 - ALPHA, 0, output_img)
    retval, buffer = cv2.imencode('.png', output_img)
    return base64.b64encode(buffer.tobytes())

# for CORS
@app.after_request
def after_request(response):
    response.headers.add('Access-Control-Allow-Origin', '*')
    response.headers.add('Access-Control-Allow-Headers', 'Content-Type,Authorization')
    response.headers.add('Access-Control-Allow-Methods', 'GET,POST') # Put any other methods you need here
    return response

if __name__ == '__main__':
    app.run(debug=True, threaded=True)
