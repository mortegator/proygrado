# -*- coding: utf-8 -*-
import configparser
import os
import sys
import cv2
import time
import numpy as np
from utils import poses2boxes
from pymongo import MongoClient
import json

# Cambiar aca el video que se esta vanalizando
VIDEO = "ch01_20181018152519.mp4"

VIDEOS_PATH = '/media/santiago/16GB1/'
POSE_DETECTIONS_PATH = '/home/santiago/Documents/fing/proygrado/cafe_out/'

# Cargar configuracion
config = configparser.ConfigParser()
config.read('config.ini')

mongodb_uri = config['database']['uri']
client = MongoClient(mongodb_uri)
db = client[config['database']['database']]

#Busca videos que no hayan sido procesados aún
videos_collection = db[config['database']['videos_collection']]
people_collection = db[config['database']['people_collection']]
detections_collection = db[config['database']['detections_collection']]

"""
Hace que las detecciones de la persona con id error_id pasen a ser asociadas a la persona real_id
"""
def fix_id(real_id, error_id,start_frame,end_frame):
    result = detections_collection.update_many(
        {
            'pid':error_id,
            'video':VIDEO,
            'frame':{
                '$gte': start_frame,
                '$lte': end_frame
            }
        },
        { '$set' :{'pid':real_id } }
    )
    print "%s resultados cambiados"%(result.modified_count)

"""
Muestra el video y deja un archivo
"""
def show():
    video_filename = VIDEO
    video_path = VIDEOS_PATH + video_filename
    output_path = "./analyzed/frombase_"+video_filename
    frame_start = 85000
    cap = cv2.VideoCapture(video_path)
    cap.set(cv2.CAP_PROP_POS_FRAMES, frame_start)
    # Define the codec and create VideoWriter object
    fourcc = cv2.VideoWriter_fourcc(*'XVID')
    #  TODO: Resolucion de video igual a al a de input!!!!
    out = cv2.VideoWriter(output_path,fourcc, 30.0, (1280,720))
    frame_i = frame_start - 1
    while(cap.isOpened()):
        frame_i += 1
        ret, frame = cap.read()
        if frame_i % 100 != 0:
            continue
        if ret==True:
            detections = list(detections_collection.find({
                'video': video_filename,
                'frame': frame_i
            }))
            for detection in detections:
                pose = detection['pose']
                kp = np.reshape(pose, (25,2)).astype(int)
                for x,y in kp:
                    cv2.circle(frame, (int(x),int(y)), 3, (255,0,0), thickness = -1)
                bbox = poses2boxes([kp])[0]
                color = (255,255,255)
                cv2.rectangle(frame, (int(bbox[0]), int(bbox[1])), (int(bbox[2]), int(bbox[3])),color, 2)
                cv2.putText(frame, "id%s "%(detection['pid']),(int(bbox[0]), int(bbox[1])-20),0, 5e-3 * 200, (0,255,0),2)

            cv2.putText(frame, str(frame_i),(40,40),0, 5e-3 * 200, (255,255,0),2)
            out.write(frame)
            cv2.imshow("output", frame)
            if cv2.waitKey(0) & 0xFF == ord('q'):
                break
        else:
            break
    cap.release()
    out.release()
