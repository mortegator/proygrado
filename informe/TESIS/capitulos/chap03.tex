\chapter{Solución propuesta}

\section{Introducción}
En esta sección se describe la solución propuesta para una herramienta de análisis de comportamiento del público a través de videos.

El objetivo es desarrollar una herramienta que permita, a través de la recopilación de videos tomados por cámaras de seguridad, un análisis del comportamiento del público en el lugar. 

% Pilares de diseño
El diseño se basa en dos pilares, por un lado garantizar una adopción del sistema fácil y de bajo costo de instalación, y por otro lado ofrecer funcionalidades que sean adaptables a diferentes escenarios de interés sin limitarse a un tipo en particular. Si bien un sistema especialmente diseñado para operar en un entorno bien definido y específico lograría un mejor rendimiento adaptándose a sus condiciones concretas, sería a costa de sacrificar su utilidad en otros escenarios y situaciones de uso. Por el contrario, la solución propuesta pretende permitir una aplicación universal que sea capaz de adaptarse a una multiplicidad de escenarios en los que es de interés realizar un análisis del comportamiento del público sin tener que limitarse al análisis de ningún tipo de conducta en particular. Esta generalidad, acompañada de un bajo costo de instalación, permite que el sistema pueda ser utilizado en diferentes escenarios como pequeñas y medianas tiendas, salas de espera de hospitales, restaurantes, oficinas de atención al público, etc.

% Pilar de costo instalación
El costo de instalación es la primer barrera al uso, ya que el usuario del sistema no verá ningún beneficio hasta que el valor aportado lo amortice. Como se mencionó anteriormente este costo se puede minimizar mediante la  utilización de videocámaras, debido a que su uso se encuentra bastante extendido y su precio de mercado es considerablemente inferior a otros sensores más sofisticados. 

% Uso de video cámaras
La mayoría de locales comerciales cuentan ya con un sistema de vigilancia consistente en una o varias cámaras de seguridad conectadas a un dispositivo central de grabación que se encarga de digitalizar la señal analógica de grabación y almacenar su contenido en un disco rígido. La solución propuesta utiliza las horas de grabación de estas cámaras de seguridad como único insumo para la detección y seguimiento de personas y el análisis de su comportamiento. Además del bajo costo de instalación ya mencionado, ofrece además mayor transparencia en su uso, ya que los visitantes del lugar no notarán ningún cambio durante su experiencia. 

% no uso de otras fuentes - Sacaría esto para que no quede tan redundante
%Con el fin de mantener la simplicidad y no interferir en la experiencia del visitante el sistema utiliza como insumo únicamente secuencias de video, renunciando a otras fuentes de datos como pueden ser la conexión WiFi o Bluetooth con el móvil del visitante. Aunque se mejoraría la calidad del seguimiento y si bien el costo de su instalación no es elevado como en el caso de otro tipos de sensores, su uso introduce una interrumpción en la experiencia del usuario, que deberá de utilizar en el móvil una aplicación especial que permita el seguimiento. La instalación de una aplicación así puede ser incentivada por el establecimiento, por ejemplo dando códigos de rebajas en los precios o información propia del lugar como ubicación de facilidades o próximos arribos, pero debe ser desarrollada especialmente para el lugar y sobre todo no puede garantizarse que la totalidad de los visitantes la utilizarán.

% Pilar de usabilidad general
El segundo pilar de gran importancia para el diseño de la solución propuesta es la universalidad de sus funcionalidades. Se pretende un sistema que pueda ser aplicado para una variedad de fines en una variedad de escenarios, como puede ser determinar el tiempo de espera de pacientes en la sala de un hospital, de compradores en la fila de un supermercado, de personas que transitan por un pasillo, etc. Se necesita dar un conjunto base de funcionalidades que puedan resolver cada situación sin necesitar un desarrollo especial de capacidades a medida. Para cumplir con este requerimiento, se propone un sistema de consultas que permite la identificación de un público objetivo mediante filtros tales como áreas recorridas y tiempos de permanencia, para luego obtener de estos la métrica que se desee. 

\section{Arquitectura}
\begin{figure}[htpb]
	\centering\includegraphics[width=.7\textwidth]{imagenes/chap3/arquitectura}
	\caption{Arquitectura de la solución propuesta}
	\label{fig:arq}
\end{figure} 
En la Figura ~\ref{fig:arq} se muestra un diagrama de la arquitectura de la solución propuesta. Los videos son inicialmente capturados a través de videocámaras ubicadas en el lugar de forma de cubrir con su campo de visión el área de interés. Los videos son guardados en formato digital por el sistema de grabación DVR (\textit{Digital Video Recording}) que se esté utilizando en un almacenamiento al que luego accede el sistema para el procesamiento de su información, que acaba del otro lado con el usuario realizando consultas sobre métricas del uso de los espacios. 

% Descripcion punto a punto del diagrama de arquitectura, resumen
Los videos son alimentados al módulo de análisis de video, que realiza en ellos la detección y seguimiento de personas. El acceso a los videos puede ser realizado en vivo si el sistema DVR permite el acceso a las cámaras mediante un \textit{streaming} o si se utiliza como almacenamiento un disco accesible por red, NAS (\textit{Network Atacched Storage} o SAN (\textit{Storage Area Network}). El análisis de la secuencia de video se realiza cuadro a cuadro, primero el detector devuelve la ubicación en la imágen de las personas encontradas y sus puntos corporales, y esas detecciones son la entrada al módulo de seguimiento, que las asociará con detecciones vistas anteriormente provenientes de las mismas personas.  La detección se realiza local en cada cuadro, y el seguimiento realizado es del tipo online, solo tiene en cuenta información de cuadros pasados. Por lo tanto, en análisis puede operar en vivo dando como salida en cada cuadro el conjunto de personas vistas, sus puntos corporales y asignando a cada persona una id única.

% DB storage
Las detecciones resultado del módulo de análisis son almacenadas en una base de datos que guarda para cada persona vista la traza de su ubicación a lo largo de la secuencia de video. Además de estas detecciones, la base también almacena algunas métricas de costo computacional elevado, por ejemplo la información de la distribución de personas vistas en cada regíon de la pantalla que se utiliza para generar el mapa de calor. Esta métrica es consultada constantemente por lo que calcularla nuevamente cada vez sería muy ineficiente. En lugar de eso, se genera bajo demanda y su resultado es almacenado en la base de datos. 

% Navegador Web
El usuario accede a la información del sistema a través de una interfaz Web. Se le presenta en ella las cámaras instaladas, y al seleccionar una puede ver un resumen de la actividad registrada, en forma de un mapa de calor, y realizar búsquedas de personas bajo ciertos criterios de selección. Una vez seleccionado el conjunto de personas objetivo, puede consultar por las métricas de su comportamiento que le sean de interés.

% Analítica
Las consultas realizadas por el usuario son resueltas por un servidor Web que contiene el módulo de analítica. Este módulo se encarga de seleccionar las detecciones almacenadas en la base que cumplan con el criterio de selección y calcular sobre ellas las métricas que el usuario haya marcado.


\section{Detalle de la arquitectura}

\subsubsection{Detección de personas}

% Intro
El módulo de detección de personas tiene como fin bien definido tomar como entrada cada una de las imágenes que conforman la secuencia de video recibida y segmentar en cada una las regiones que corresponden a personas. 

% Razon para elegir OpenPose
Si bien cualquier detector de personas que localice puntos corporales podría utilizarse en este módulo, se utiliza OpenPose en la solución propuesta por su buena performance y su simplicidad. OpenPose ha logrado una gran adopción y ha generado una comunidad de colaboración y desarrollo activa y en crecimiento, lo que asegura que continuarán lanzando nuevas versiones y añadiendo funcionalidades. Desde su lanzamiento en Abril del 2017 se han liberado 10 nuevas versiones de OpenPose, y se han añadido características como nuevos modelos corporales, reconstrucción 3d y mejoras al rendimiento. Es necesario mencionar de todas formas que el uso comercial de OpenPose está sujeto a licencias, mientras que es gratuito para cualquier otro tipo de proyectos\footnote{Ver https://github.com/CMU-Perceptual-Computing-Lab/openpose\#license}.

% Ruido en Openpose
Las detecciones obtenidas por OpenPose son ruidosas y poco estables. Puede ocurrir que se obtengan falsas detecciones de partes corporales y hasta de personas enteras por varios cuadros seguidos, o que la asociación entre partes detectadas y personas se errónea por momentos. Estas imperfecciones en la detección pueden ser vistas como ruido en la detección del instrumento, que puede ser reducido al aplicar el filtrado de Kalman en el módulo de seguimiento posterior.


\subsubsection{Seguimiento de personas}
% Intro

El módulo de seguimiento toma para cada cuadro las detecciones y asocia cada una a otras detecciones vistas en cuadros anteriores. Así, un conjunto de detecciones asociadas conforman la traza de una persona, la información de cómo se movió cada uno de sus puntos corporales por el campo de cobertura de la videocámara y que será almacenada en la base de datos para su posterior consulta.

% Porque usar deepsort
Se utiliza para el seguimiento el algorito Deep SORT, que implementa un seguimiento por detección online, tomando como características la ubicación y velocidad de los objetos así como también información de su apariencia. El diseño de Deep SORT logra ser eficiente al mismo tiempo que mantiene una gran simplicidad, y la utilización de información de apariencia lo hace idóneo para el seguimiento de personas, en el que la capacidad de resolver correctamente oclusiones temporales y superposiciones entre personas es de vital importancia para una buena calidad del seguimiento realizado.

% Cajas delimitadoras
Deep SORT representa los objetos mediante su ubicación en la imágen, su ancho y su largo. Generalmente, estas dimensiones corresponde a la de la caja delimitadora del objeto detectado, es decir el mínimo cuadrado en pantalla que abarca la totalidad del objeto. Las personas detectadas por OpenPose son representadas como el conjunto de puntos correspondiente a cada una de sus partes corporales vistas, por lo que es necesario fijar un criterio para convertir este conjunto de puntos a la representación rectangular de Deep SORT. 

% Lo malo del criterio de caja simple
La forma más natural de convertir el dominio de salida de OpenPose al de entrada de Deep SORT sería tomar un rectángulo cuyas coordenadas sean los puntos máximos y mínimos en cada eje del conjunto de puntos corporales detectadas. Este sería el cuadrado mínimo que incluiría a todos los puntos, similar al usado generalmente en problemas de segmentación. Sin embargo, las pruebas con este criterio de conversión demostraron que genera mucha inestabilidad en las dimensiones de las cajas delimitadoras, lo que termina empeorando la performance del seguimiento. Los puntos extremos en la pose corresponden la mayor parte del tiempo a manos y piernas, que a su vez son los que más movimiento suelen presentar. Así, si una persona sentada levanta su mano, el largo del rectángulo delimitador de sus detecciones será duplicado repentinamente, lo que generará una velocidad en el estado interno del filtro de Kalman. Estas deformaciones en las dimensiones agregan un ruido permanente al filtrado que empeora su rendimiento, siendo mejor que las dimensiones de los objetos seguidos permanezcan lo mas estables posibles.

% El criterio de caja propuesto. 
Para lograr dimensiones mas estables en los rectángulos seguidos se propone un criterio de construcción alternativo que en lugar de abarcar todos los puntos procura delimitar la mayor cantidad de puntos centrales, bajo el supuesto de que son esos los que mayor estabilidad en su ubicación tendrán.

El rectángulo delimitador de cada detección queda determinado por sus dimensiones $r=[u,v,w,l]$ donde $u$ y $v$ corresponden a las coordenadas de su centro en el eje x e y, y $w$ y $l$ a su ancho y largo. El centro $u$, $v$ se calcula como el valor medio de los puntos en cada eje:

\begin{equation}
u = \frac{\sum_{p_{i} \in P_{j}}p_{i}^{x}}{\left \| P_{j} \right \|}
\end{equation}
\begin{equation}
v = \frac{\sum_{p_{i} \in P_{j}}p_{i}^{y}}{\left \| P_{j} \right \|}
\end{equation}

siendo $P_{j} = {p_{1}...p_{n}}$ el conjunto de puntos corporales detectados para la persona $j$, donde $||P|| = n$. Esto ubica el centro del rectángulo en el punto medio entre todas las detecciones. El rectángulo se termina de delimitar sumando de cada lado del centro la desviación estándar de los puntos en cada eje, por lo que el ancho $w$ y el largo $l$ se calculan:
\begin{equation}
w = 2 \sqrt{\frac{\sum_{p_{i}\in P}(p_{i}^{x}-u)^{2}}{\left \| P \right \|}}
\end{equation}
\begin{equation}
l = 2 \sqrt{\frac{\sum_{p_{i}\in P}(p_{i}^{y}-v)^{2}}{\left \| P \right \|}}
\end{equation}

% decir porque no usar un criterio de eleccion de puntos por tipo
Si bien podrían adoptarse otros criterios de construcción del rectángulo que usasen información cualitativa de los puntos, por ejemplo tomando solo los puntos de ciertas partes del cuerpo o ponderando según su tipo, se lograrían resultados semejantes al costo de una complejidad adicional.


\begin{figure}
	\begin{minipage}{\linewidth}
		\includegraphics[width=.32\linewidth]{imagenes/chap3/gorrasentado}\hfill
		\includegraphics[width=.28\linewidth]{imagenes/chap3/tipoparado}\hfill
		\includegraphics[width=.3\linewidth]{imagenes/chap3/falsapierna}%
	\end{minipage}%
	\caption{Comparación entre cajas delimitadoras.}
	\label{fig:cajas}
\end{figure}

% Referencia a las imagenes ( cambiar orden )
En la Figura ~\ref{fig:cajas} se puede ver la diferencia entre aplicar ambos criterios de delimitación de cajas contenedoras. Los rectángulos rojos corresponden a la delimitación mediante puntos máximos, y los verdes al criterio propuesto. Como se observa, las dimensiones del rectángulo siempre son menores, y cubren mayoritariamente el centro del cuerpo de la persona mientras que dejan por fuera los puntos de las extremidades. En la tercera figura, se le asocia incorrectamente al cuerpo de la persona sentada una pierna de otra persona; la caja delimitadora por puntos máximos expande su tamaño para incluir este punto, mientras que la caja resultado del criterio propuesto simplemente lo deja por fuera. Si un cuadro siguiente el error en la detección es corregido, el rectángulo verde permanecerá estable, mientras que el rojo cambiará considerablemente su tamaño.

% Parametrizacion
El filtro de Kalman utilizado por Deep SORT para el seguimiento de objetos debe ser parametrizado a las condiciones del problema en que se quiere aplicar. Las matrices de observación, transición y sus covarianzas deben de ser especificadas. Algunas de estas matrices son específicas al problema de seguimiento de cajas delimitadoras, como la matriz de transición de estados, que asocia cada dimensión a su correspondiente velocidad, la matriz de control $B$, que no es utilizada, y la matriz de observación $H$. Las matrices de covarianza por otro lado pueden ser personalizables a cada escenario si se tiene algún conocimiento a priori sobre el tipo demovimiento que realizarán los objetos a seguir. 

% En que escenarios usar la parametrizacion
Las matrices de covarianza en la transición y observación pueden utilizarse para regular la incidencia de las nuevas observaciones en el estado interno del objeto seguido, haciendolo más o menos suceptible a cambios. Según el escenario puede ser conveniente regular esta parametrización para reducir esta incidencia, si se sabe que los objetos seguidos tienden a mantener su posición o cambiarla levemente, o incrementarla si presentan gran dinamismo. 

% Salida del módulo de seguimiento
El módulo de seguimiento asocia entonces las detecciones en cada cuadro con otras vistas anteriormente, asignando un mismo id único al seguimiento de cada persona vista. Estos seguimientos son almacenados en la base, donde se puede consultar para una persona cuál fue la traza de su movimiento, o para un área de la imágen cuáles fueron las personas que pasaron por ahí.


\subsubsection{Análisis de las detecciones}
% Intro
El módulo de Análisis es el encargado de tomar las consultas realizadas por el usuarios y encontrar las detecciones de personas que cumplan con los criterios ingresados. Sobre este conjunto de personas encontradas, el usuario podrá realizar filtrados adicionales o consultar por métricas que le interese conocer. 

% Requisisto
Se necesita de un mecanismo de ingreso de las consultas que sea lo suficientemente expresivo de forma que el usuario pueda crear diferentes criterios de selección de su interés en distintos tipos de escenarios, y que sea también simple de usar y de entender su funcionamiento. 

% Filtros por área
Se propone expresar las consultas en forma de una concatenación de filtros por zonas, dónde cada filtro corresponde a un área de la pantalla. Las personas seleccionadas por la consulta serán aquellas para las cuales existen detecciones observadas que estén dentro de alguna de las zonas delimitadas. Así, la forma de ingresar una consulta por el usuario es dibujando en el plano de imagen de la escena capturada por la videocámara un polígono dentro del cual deberán haber sido vistas en algún momento las personas buscadas.

% Filtros por tiempo
El usuario debe poder además filtrar adicionalmente las personas imponiendo un tiempo de permanencia máximo o mínimo o un intervalo temporal de interés. Esto, además de dar más herramientas al usuario para realizar consultas más precisas, es un requerimiento necesario en caso de que la base de grabaciones sea muy extensa, para evitar consultas que requieran demasiado tiempo de procesamiento.

% Concatenacion, negacion
Mediante la concatenación de varios polígonos de búsqueda, el usuario puede formar consultas más complejas, como seleccionando las personas que hayan pasado por una zona y luego por otra. Los filtros también pueden ser acompañados de operadores lógicos, para poder expresar consultas como la búsqueda de personas que hayan pasado por una zona pero no por otra. Finalmente, el usuario puede especificar si el orden de los filtros debe ser respetado o si no es relevante, es decir si solo se quiere seleccionar a las personas que fueron vistas pasando por una zona A y luego por una zona B, o si cualquier orden de visita es suficiente para su selección.

% Eficiencia de la búsqueda
Una vez ingresado desde el sistema Web el criterio de búsqueda, la consulta es enviada al módulo de análisis por intermedio de una interfaz REST, el cual buscará en la base de datos las trazas de personas cuyas detecciones cumplan con las condiciones impuestas. Se trata de una búsqueda de gran costo computacional ya que su complejidad impide el uso de índices en campos de registros para incrementar la velocidad de selección. Todas las personas vistas son potencialmente seleccionables, es necesario iterar sobre cada una de ellas y aplicar los criterios de selección a sus detecciones para determinar si es seleccionable o no. El tiempo de ejecución de la consulta incrementa con la cantidad de horas de video analizadas y la cantidad de detecciones en ellas vistas.

% Métricas sobre la selección
Finalizada la búsqueda, el conjunto de personas seleccionadas es mostrado al usuario. Se muestra la cantidad de personas encontradas y el tiempo de permanencia de cada una de ellas.

En la sección~\ref{Prototipo} se presenta un prototipo de la solución propuesta, incluyendo capturas de pantalla que ilustran la interfaz Web para consultas.

\subsection{Búsqueda de poses}
% Intro con ejemplos
La detección de los puntos corporales para las personas detectadas en cada uno de los cuadros de la secuencia de video realizada por OpenPose abre la posibilidad de buscar poses corporales que sean de interés para el usuario de la plataforma. De esta forma, el sistema podría ofrecer tambien la posibilidad de agregar a la consulta filtros que seleccionen personas cuya posición corporal sea semejante a una de interés. Por ejemplo, podría ser de interés detectar las personas que estuvieron sentadas en una zona, o las que extendieron sus manos hacia un objeto.

% Eso si usa clasificaicon de puntos
Tanto para el seguimiento de las personas (realizado a través de sus cajas contenedoras) como para los criterios de búsqueda por posición descritos en las secciones anteriores, los puntos corporales detectados eran utilizados solamente para determinar la posición de la persona en la imagen, solo se tomaba la información de su ubicación sin importar el punto antómico al que correspondían. La clasificación de puntos corporales es utilizada al realizar una búsqueda de pose corporal, justificando asi el uso de OpenPose sobre otro detector que solo haga una detección de personas sin aportar información sobre su pose.

% Ingresar puntos en interfaz
OpenPose representa la pose humana mediante 25 puntos, por lo que definir un criterio de búsqueda de una pose objetivo equivale a determinar cual es la posición relativa de cada uno de estos puntos. Para brindar al usuario una forma intuititva de realizar esto desde la interfaz gráfica, se consideraron dos posibles maneras. Una posibilidad es representar los 25 puntos en pantalla mediante un esqueleto con el cual el usuario pueda interactuar para ajustar la posición de cada punto corporal hasta lograr determinar la pose buscada. Si bien de esta forma es posible determinar cualquier posición, el uso puede resultar complicado a una persona que no esté familiarizada con el diseño 3D y las dificultades de mover la vista a través de los ejes, comprender la perspectiva de la cámara, etc. 

% Ingresar puntos por similaritud
Una segunda opción sería determinar la pose objetivo a través de una semejanza con otra pose vista ya en el video para alguna persona. Así, el usuario debería moverse por la secuencia grabada hasta encontrar una persona que haya realizado el comportamiento que es de su interés y crear un criterio de búsqueda en el que se filtrarían las personas a las que se haya detectado una pose similar en algún momento. Este método sería mucho más ágil que el primero, pero como contrapartida limitaría la expresividad de la consulta a poses que ya hayan sido vistas anteriormente. En todo caso, ambos métodos de búsqueda no son excluyentes y el sistema podría implementar ambos y permitir al usuario elegir el que prefiera utilizar.


\section{Prototipo realizado} \label{Prototipo}

Con el fin de comprobar la viabilidad técnica del sistema propuesto, fue construido un prototipo con un conjunto mínimo de funcionalidades. El objetivo fue validar que la tecnología disponible hoy en día para la detección de personas y su seguimiento es suficiente como para construir la plataforma ideada, y evaluar la calidad de los resultados luego de su aplicación en un escenario en particular.

La viabilidad del proyecto depende fuertemente de la calidad lograda en la detección y el seguimiento. Un alto margen de error en cualquiera de las dos implicaría una distorsión en las métricas obtenidas que acabaría por quitarles cualquier utilidad. El prototipo construido tiene como principal objetivo relevar la calidad obtenida y el impacto que los errores de detección provocan sobre las métricas de comportamiento de las personas. 

% resto de funcionalidades? -> Tiempo de ejecución

% Openpose y Deep SORT
Se utilizó para la detección la versión 1.3.0 de OpenPose con una resolución de 656x368. El modelo de cuerpo humano utilizado es BODY\_25, sin incluir en la detección los puntos correspondientes a manos y caras, que introducen una degradación significativa en la performance. La detección es ejecutada sobre archivos de videos ya capturados. Se captura cada cuadro con OpenCV\footnote{Por más información sobre OpenCV, ver \textit{https://opencv.org/}}, y se le aplica OpenPose, que genera como salida, para cada cuadro, un archivo en formato JSON con las personas vistas y las coordenadas en pantalla para los puntos corporales detectados. Estos archivos de detecciones son alimentados al módulo de seguimiento, construido con la implementación de Deep SORT disponible en su repositorio \cite{DeepSortRrepo}. Deep SORT fue modificado para utilizar en lugar de la implementación del filtro de Kalman con la que es distribuido, una diferente utilizando la librería PyKalman \cite{pykalman}, que tiene un diseño más modular y permite una parametrización más fácil del filtrado.

% MongoDB
Las detecciones son almacenadas en una base de datos no relacional MongoDB\footnote{Por más información sobre MongoDB, ver \textit{https://www.mongodb.com/}}. MongoDB almacena los datos en documentos de formato JSON\footnote{Por más información sobre el formato JSON, ver \textit{https://www.json.org/}}, organizándolos en diferentes colecciones. Cada detección es almacenada en un documento diferente, registrando los puntos corporales observados y el ID de persona que se le asignó en el seguimiento. La utilización de una base no relacional facilita el prototipado al no ser necesaria una definición estática de la estructura de la base y de los tipos de datos almacenados. 

% Flask y python para análisis
El módulo de análisis expone en una interfaz REST\footnote{Por más información sobre APIs de tipo REST, ver \textit{https://restfulapi.net/}} los servicios de búsqueda y consulta de métricas. Se utilizó el framework Flask\footnote{Por más información sobre Flask, ver \textit{http://flask.pocoo.org/}} escrito en Python, que gracias a un diseño minimalista permite crear rápidamente aplicaciones Web. El usuario ingresa las consultas a través de una interfaz Web desarrollada en Angular 6\footnote{Por más información sobre Angular, ver https://angular.io/}.

% Que parte de la búsqueda fue implementada
Para mantener la simplicidad del prototipo y acotar los tiempos de desarrollo, la búsqueda fue limitada a un solo polígono. Si bien la potencialidad de búsqueda se ve limitada y se restringe al usuario a indicar una sola área de interés, es suficiente para evaluar el desempeño del sistema de búsqueda. En una búsqueda compuesta de una concatenación de filtros, será el primer filtro el que mayor costo computacional implique, pues todas las detecciones almacenadas en la base de datos son posibles candidatos. Los sucesivos filtros se aplican solo a los resultados de los filtros anteriores, por lo que el costo computacional será considerablemente menor.

%Interfaz web
La interfaz Web de búsqueda presenta al usuario un cuadro vacío (es decir, sin personas), tomado de los videos disponibles que conforman el dataset utilizado para la evaluación, como se describe en dicha sección. El usuario tiene la posibilidad en ese momento de dibujar, mediante un polígono, el área que le interesa consultar. El sistema buscará entonces todas las personas que fueron detectadas en esa zona, y retornará los siguientes resultados:
\begin{itemize}
	\item un listado de personas detectadas en esa zona, identificados por su ID, e indicando la cantidad de minutos que permanecieron en la misma
	\item un histograma que resume la permanencia en minutos en función de la cantidad de personas detectadas
	\item filtros adicionales por tiempo, que permiten al usuario filtrar los resultados según un rango de fechas, o por permanencia mínima y/o máxima
\end{itemize}

La Figura~\ref{fig:interfaz-web-busqueda} presenta una ilustración sobre la interfaz disponible al usuario, mientras que la Figura~\ref{fig:int-web-resultados} ilustra la presentación de los resultados una vez efectuada la búsqueda.

\begin{figure}[H]
	\centering\includegraphics[width=\textwidth]{imagenes/chap3/interfaz-web}
	\caption{Ilustración de la interfaz Web de consulta disponible al usuario. El área de interés dibujada es la delimitada por el polígono rojo. La numeración de las mesas fue realizada a los efectos de la evaluación, como se describe más adelante en dicha sección.}
	\label{fig:interfaz-web-busqueda}
\end{figure}

\begin{figure}[!htb]
	\begin{minipage}{0.48\textwidth}
		\centering\includegraphics[height=8cm,width=1\textwidth]{imagenes/chap3/int-web-histograma}
	\end{minipage}
	%\hfill\vline\hfill
	\begin{minipage}{0.48\textwidth}
		\centering\includegraphics[height=3.5cm,width=1\textwidth]{imagenes/chap3/int-web-filtros}
		\centering\includegraphics[height=4.5cm,width=1\textwidth]{imagenes/chap3/int-web-personas}
	\end{minipage}
	%\minipage{0.32\textwidth}%
	
	%\endminipage
	\caption{Ilustración de lo que retorna la interfaz Web al usuario luego de completada una búsqueda. A la izquierda se presenta un histograma que resume la permanencia de las personas. A la derecha, arriba, se ilustran los filtros por tiempo que el usuario puede utilizar para refinar los resultados, y a la derecha, abajo, se enumeran las personas que fueron detectadas en la zona consultada}
	\label{fig:int-web-resultados}
\end{figure}

% No se implemento la búsqueda de pose en interfaz
La búsqueda de poses fue implementada directamente en un cuaderno Jupyter con Python, tomando las detecciones directamente de la base de datos. No fue implementado en el prototipo una interfaz gráfica de búsqueda de pose. 

% Presentación siguiente sección
En la siguiente sección se presentan los resultados obtenidos en la utilización de este prototipo desarrollado para el análisis de un conjunto de videos de prueba.

