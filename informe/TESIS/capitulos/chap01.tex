\chapter{Introducción}

\section{Motivación}
% computer vision
La visión artificial, o visión por computador, es una rama de la ingeniería que trata el problema de cómo pueden las computadoras, u otros sistemas, emular la percepción visual humana. Si bien es un área que involucra múltiples disciplinas, su principal objetivo puede resumirse como el de construir sistemas artificiales que sean capaces de lograr una interpretación de alto nivel en imágenes o videos, y que dichos sistemas puedan utilizarse para automatizar tareas que la visión humana es capaz de realizar.

% deep learning & computer vision
Si bien los orígenes de la visión artificial se remontan a 1960 \cite{comp-vision}, en los últimos años ha ganado especial atención, gracias en gran parte al éxito logrado al combinar su aplicación con modelos de aprendizaje automático. El punto de quiebre puede considerarse el año 2012, cuando Alex Krizhevsky \cite{krizhevsky} introdujo el uso de redes neuronales convolucionales para la clasificación de imágenes, superando ampliamente el rendimiento de los modelos basados en técnicas tradicionales que conformaban el estado del arte del momento. Desde entonces, el crecimiento de los modelos de aprendizaje profundo llevó a que desplazaran rápidamente a los métodos tradicionales, basados en técnicas como la extracción manual de características, logrando mejorar año a año el desempeño en las competencias de reconocimiento visual hasta llegar a superar el rendimiento humano en dicha tarea \cite{outperform}.

% aplicaciones que se han desbloqueado
La utilización del aprendizaje profundo en problemas de visión artificial ha sido adoptada exitosamente, no solo para la clasificación de imágenes, sino también para tareas como la detección de objetos (es decir, localizar qué partes de una imagen corresponden a objetos de un determinado tipo), segmentación semántica (etiquetar cada pixel de una imagen) y en generación de leyendas a partir de una imagen (producir un texto descriptivo de su contenido) \cite{generaldeep}. En la industria, sus más notables aplicaciones podrían resumirse en el desarrollo de vehículos de conducción automática, así como también en el análisis automático de radiografías médicas. De forma esperable, los emprendimientos relacionados a solucionar problemas a través de visión por computadora atraen cada año más financiamiento, teniendo una valoración promedio de \$5,2 millones de dólares en el mercado estadounidense \cite{angellist}.

% detección y seguimiento de personas 
Entre las múltiples aplicaciones que la visión por computadora combinada con el aprendizaje profundo pueden tener, de las cuales solo hemos mencionado un subconjunto, nos resulta de particular interés centrarnos en los problemas de detección de personas en imágenes. 
Delimitando la problemática con aún más precisión, lo que nos interesa abordar es la localización de personas, y sus puntos corporales, a lo largo de una grabación de video. Un sistema que combine la detección de una persona con el seguimiento de la misma, lograría registrar los movimientos de todas las personas presentes en el campo de visión de una cámara a lo largo de una secuencia de video.

% propuesta, utilizar la detección de personas para algo
La mayoría de las soluciones propuestas para esto implican soporte de hardware adicional, como sensores infrarrojos o cámaras termales, lo que complejiza su implementación e implantación. La posibilidad de detectar personas en videos, sin mayor soporte que el de las cámaras que los registran, incrementa considerablemente sus posibilidades de adopción, y abre además un gran abanico de potenciales aplicaciones. Simplemente por mencionar algunos ejemplos, un sistema con estas características podría utilizarse para disparar alarmas ante intrusiones, realizar un conteo de clientes en una góndola de un supermercado, o integrarse con funciones de domótica en el hogar para disparar una acción cuando una persona ingresa a una habitación, o simplemente realiza un gesto de interés.

% cámaras ya instaladas generan datos que puedan ser utilizados
Si bien la mayoría de tiendas comerciales y espacios públicos en general tienen ya instalado un sistema de videocámaras de seguridad que guarda un registro de lo ocurrido en el lugar, la utilidad que se le da a esta información recabada está limitada solamente a funciones de vigilancia. Normalmente, los registros solo son consultados como evidencia cuando ocurre algún siniestro o hurto, y son descartados en el resto de situaciones cotidianas. Sin embargo, esta información de uso del lugar registrada guarda potencial no explorado para comprender mejor la utilización que el público le da un espacio físico y cuales son sus patrones de comportamiento.

\section{Propuesta}
% que se propone
En este trabajo se propone entonces investigar la posibilidad de desarrollar un sistema que, aplicando técnicas recientes para la detección y seguimiento de personas, permita analizar un conjunto de secuencias de video tomadas por cámaras de seguridad para estudiar el comportamiento del público en el lugar. Así, en lugar de desperdiciar la información recabada constantemente por las videocámaras ya instaladas, se explota para el estudio de comportamientos que sean de interés para, por ejemplo, el dueño del establecimiento.

% Múltiples escenarios
La variedad de aplicaciones incluso en este escenario delimitado son grandes. Podría por ejemplo resultar de interés contar el tiempo de espera de los pacientes en una sala de espera antes de ser atendidos, la cantidad de personas en la fila de una caja en una sucursal bancaria según el momento del día, el uso de los pasillos de una estación de trenes, etc. 

% plataforma de uso general
Un sistema que ofrezca un conjunto de funcionalidades con un alto grado de generalidad, que permitan realizar un análisis del uso de un lugar independientemente de sus características, resultaría interesante. Un sistema así debería ofrecer una serie de herramientas con las que se puedan cubrir diferentes casos de uso, sin tener que desarrollar para cada uno una funcionalidad específica. 

% evaluar en condiciones no controladas
Nos interesa puntualmente investigar la viabilidad de un sistema con estas características, utilizando las herramientas de visión artificial disponibles hoy en día. Para esto evaluamos el rendimiento alcanzado por los métodos últimamente propuestos para la detección y el seguimiento de personas en condiciones no controladas. Los sistemas presentados en publicaciones académicas son, en su gran mayoría, evaluados en conjuntos de datos preexistentes, con el fin de poder comparar directamente su rendimiento frente a otros. Por el contrario, en este trabajo nos interesa evaluar el rendimiento alcanzado por el estado de arte en una situación no controlada, en la que las condiciones del ambiente, como la ubicación de las videocámaras o la disposición del mobiliario, no puedan ser modificadas a conveniencia.

\section{Organización del documento}
% Presentación de secciones
Este documento expone la investigación realizada, la solución propuesta y los resultados obtenidos por el prototipo realizado a lo largo de cinco capítulos, que comienzan en esta introducción.

En el Capítulo 2 se describen los fundamentos teóricos de los problemas de detección y seguimiento de personas y se presentan los métodos del estado del arte que serán utilizados en el sistema propuesto. La presentación de estos dos problemas que serán los pilares del sistema propuesto, es acompañada por un breve estudio del problema de análisis del público en espacios, sus utilidades y otras soluciones propuestas. 

En el Capítulo 3 se presenta la solución propuesta para la implementación de un sistema que permita el análisis del comportamiento de personas en secuencias de video. Se describe su arquitectura general, cómo se integran en esta los métodos de detección y seguimiento utilizados, y se presentan sus funcionalidades y cómo estas pueden ser adaptadas a una variedad de escenarios posibles. Para evaluar la calidad de la solución propuesta, se realiza un prototipo de sus funcionalidades mínimas, que es presentado al final de la sección.

Los resultados obtenidos en la evaluación del prototipo son presentados en el Capítulo 4. En él se describe el conjunto de datos utilizado, sus características, y el procedimiento para su evaluación.

Finalmente, en el Capítulo 5 se presentan las conclusiones del trabajo, evaluando la viabilidad, y oportunidades de mejora del sistema a partir de los resultados obtenidos. 

