\babel@toc {spanish}{}
\contentsline {chapter}{\numberline {1}Introducci\IeC {\'o}n}{1}{chapter.1}
\contentsline {section}{\numberline {1.1}Motivaci\IeC {\'o}n}{1}{section.1.1}
\contentsline {section}{\numberline {1.2}Propuesta}{3}{section.1.2}
\contentsline {section}{\numberline {1.3}Organizaci\IeC {\'o}n del documento}{4}{section.1.3}
\contentsline {chapter}{\numberline {2}Fundamentos te\IeC {\'o}ricos}{5}{chapter.2}
\contentsline {section}{\numberline {2.1}An\IeC {\'a}lisis de p\IeC {\'u}blico en espacios}{5}{section.2.1}
\contentsline {section}{\numberline {2.2}Detectores de personas}{7}{section.2.2}
\contentsline {subsection}{\numberline {2.2.1}Deformable Parts Model}{8}{subsection.2.2.1}
\contentsline {subsection}{\numberline {2.2.2}OpenPose}{11}{subsection.2.2.2}
\contentsline {subsubsection}{M\IeC {\'e}todo}{12}{section*.4}
\contentsline {subsubsection}{Detecci\IeC {\'o}n y Asociaci\IeC {\'o}n simult\IeC {\'a}neas}{12}{section*.6}
\contentsline {subsubsection}{Mapas de confianza para la detecci\IeC {\'o}n de partes}{14}{section*.8}
\contentsline {subsubsection}{Campos de afinidad para la asociaci\IeC {\'o}n entre partes}{14}{section*.9}
\contentsline {subsubsection}{Parsing multi-persona utilizando campos de afinidad entre partes}{17}{section*.12}
\contentsline {subsubsection}{Evaluaci\IeC {\'o}n del m\IeC {\'e}todo al momento de su lanzamiento}{18}{section*.13}
\contentsline {subsection}{\numberline {2.2.3}Estado del arte}{20}{subsection.2.2.3}
\contentsline {subsubsection}{Associative embedding}{21}{section*.17}
\contentsline {subsubsection}{RMPE: Regional Multi-Person Pose Estimation}{22}{section*.18}
\contentsline {subsubsection}{PoseRefiner}{22}{section*.19}
\contentsline {section}{\numberline {2.3}Seguimiento de objetos}{23}{section.2.3}
\contentsline {subsection}{\numberline {2.3.1}Definici\IeC {\'o}n del problema}{23}{subsection.2.3.1}
\contentsline {subsection}{\numberline {2.3.2}Seguimiento por detecci\IeC {\'o}n (DBT)}{25}{subsection.2.3.2}
\contentsline {subsubsection}{Detecci\IeC {\'o}n}{25}{section*.21}
\contentsline {subsubsection}{Predicci\IeC {\'o}n}{26}{section*.22}
\contentsline {subsubsection}{Asociaci\IeC {\'o}n}{29}{section*.23}
\contentsline {subsubsection}{M\IeC {\'e}tricas de evaluaci\IeC {\'o}n}{30}{section*.24}
\contentsline {subsection}{\numberline {2.3.3}Algoritmos de seguimiento}{31}{subsection.2.3.3}
\contentsline {subsection}{\numberline {2.3.4}SORT}{32}{subsection.2.3.4}
\contentsline {subsection}{\numberline {2.3.5}Deep SORT}{34}{subsection.2.3.5}
\contentsline {subsection}{\numberline {2.3.6}Comparaci\IeC {\'o}n de resultados}{36}{subsection.2.3.6}
\contentsline {chapter}{\numberline {3}Soluci\IeC {\'o}n propuesta}{38}{chapter.3}
\contentsline {section}{\numberline {3.1}Introducci\IeC {\'o}n}{38}{section.3.1}
\contentsline {section}{\numberline {3.2}Arquitectura}{39}{section.3.2}
\contentsline {section}{\numberline {3.3}Detalle de la arquitectura}{41}{section.3.3}
\contentsline {subsubsection}{Detecci\IeC {\'o}n de personas}{41}{section*.28}
\contentsline {subsubsection}{Seguimiento de personas}{42}{section*.29}
\contentsline {subsubsection}{An\IeC {\'a}lisis de las detecciones}{45}{section*.31}
\contentsline {subsection}{\numberline {3.3.1}B\IeC {\'u}squeda de poses}{46}{subsection.3.3.1}
\contentsline {section}{\numberline {3.4}Prototipo realizado}{47}{section.3.4}
\contentsline {chapter}{\numberline {4}Evaluaci\IeC {\'o}n de la soluci\IeC {\'o}n}{52}{chapter.4}
\contentsline {section}{\numberline {4.1}Conjunto de datos utilizados}{52}{section.4.1}
\contentsline {section}{\numberline {4.2}An\IeC {\'a}lisis de permanencia de personas}{54}{section.4.2}
\contentsline {subsection}{\numberline {4.2.1}Procedimiento de evaluaci\IeC {\'o}n}{55}{subsection.4.2.1}
\contentsline {subsection}{\numberline {4.2.2}Resultados de la evaluaci\IeC {\'o}n}{57}{subsection.4.2.2}
\contentsline {subsubsection}{Generaci\IeC {\'o}n de base de datos sin errores}{59}{section*.41}
\contentsline {subsubsection}{Resultados obtenidos luego de la correcci\IeC {\'o}n}{60}{section*.42}
\contentsline {subsection}{\numberline {4.2.3}Conclusiones}{62}{subsection.4.2.3}
\contentsline {section}{\numberline {4.3}B\IeC {\'u}squeda de pose}{64}{section.4.3}
\contentsline {subsection}{\numberline {4.3.1}B\IeC {\'u}squeda por reglas}{65}{subsection.4.3.1}
\contentsline {subsection}{\numberline {4.3.2}B\IeC {\'u}squeda por semejanza}{65}{subsection.4.3.2}
\contentsline {subsection}{\numberline {4.3.3}Procedimiento de evaluaci\IeC {\'o}n}{67}{subsection.4.3.3}
\contentsline {subsection}{\numberline {4.3.4}Resultados obtenidos}{68}{subsection.4.3.4}
\contentsline {chapter}{\numberline {5}Conclusiones y trabajos a futuro}{73}{chapter.5}
\contentsline {chapter}{Referencias bibliogr\IeC {\'a}ficas}{78}{chapter*.53}
\contentsfinish 
