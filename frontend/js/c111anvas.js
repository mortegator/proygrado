var canvas = new fabric.Canvas('canvas');
const ctx = canvas.getContext('2d');

//canvas.setHeight(500);
//canvas.setWidth(1300);

/*canvas.style.width ='100%';
canvas.style.height='738px';
canvas.width = canvas.offsetWidth;
canvas.height = canvas.offsetHeight;*/

draw_emptyframe();

//Función que busca en el server las detecciones que coincidan con la trayectoria dibujada por el usuario
function search_user_trace(){
    $.ajax({
        data:  JSON.stringify(trace),
        url:   "http://127.0.0.1:5000/query_path",
        type:  'POST',
        headers: {
            'Content-Type': 'Application/JSON'
        },
        success:  function (paths) {
            paths.forEach(function(path){
                $("#resultados").append('<li class="nav-item"><a class="nav-link" href="#")><span data-feather="user"></span>' + path[0] + '</a></li>');
                $("#resultados > li:last-child > a").click(function(){
                    get_trace(path[0]);
                });
            })
            feather.replace();
        }
    });
}

//Función que trae desde el servidor la trayectoria de la persona pasada como parámetro
function get_trace(id){
    $.get("http://127.0.0.1:5000/midpoint_trace/" + id, function(path, status){
        ctx.fillStyle = 'blue';
        path["trace"].forEach(function(entry){
            ctx.beginPath();
            ctx.arc(entry["midpoint"][0], entry["midpoint"][1], radius/2, 0, Math.PI*2);
            ctx.fill();
        })
        ctx.beginPath();
    })
}

//Función que trae el frame vacío desde el server y lo dibuja en el canvas
function draw_emptyframe(){
    $.get("http://127.0.0.1:5000/scene/1", function(data, status){
        base_image = new Image();
        base_image.src = "data:image/png;base64," + data;
        base_image.onload = function(){
            ctx.drawImage(this, 0, 0, canvas.width, canvas.height);
            canvas.setBackgroundImage(base_image, canvas.renderAll.bind(canvas),{
                originX: 'left',
                originY: 'top'
            })
        }
    });
}

//Función que limpia el dibujo y los resultados
function reset_canvas(){
    //ctx.clearRect(0, 0, canvas.width, canvas.height);
    $("#canvas").fadeOut(300, function (){
        draw_emptyframe();
        $("#canvas").fadeIn(200);
    })
    $("#resultados").empty();
    drawing_enabled = true;
}

$("#search_path").click(search_user_trace);
$("#reset_canvas").click(reset_canvas);
