var canvas = document.getElementById('canvas');
const ctx = canvas.getContext('2d');

canvas.style.width ='100%';
canvas.style.height='738px';
canvas.width = canvas.offsetWidth;
canvas.height = canvas.offsetHeight;

draw_emptyframe();
var trayectorias;

/* ------ DIBUJO ------ */
var radius = 10;

//variable para controlar si el usuario está arrastrando el mouse
var dragging = false;
var drawing_enabled = true;
var trace = [];

var putPoint = function(e){
    if (dragging){
        trace.push([e.offsetX, e.offsetY]);
        ctx.lineTo(e.offsetX, e.offsetY);
        ctx.stroke();
        ctx.beginPath();
        ctx.arc(e.offsetX, e.offsetY, radius, 0, Math.PI*2);
        ctx.fill();
        ctx.beginPath();
        ctx.moveTo(e.offsetX, e.offsetY);
    }
}

//Cuando el usuario hace click empiezo a dibujar puntos
var engage = function(e){
    ctx.lineWidth = 2*radius;
    ctx.strokeStyle = 'black';
    ctx.fillStyle = 'black';
    if (drawing_enabled) {
        drawing_enabled = false;
        dragging = true;
        trace = [];
        putPoint(e);
    }
}

//Cuando el usuario suelta el mouse dejo de dibujar
var disengage = function(){
    dragging = false;
    ctx.beginPath();
}

//Función que busca en el server las detecciones que coincidan con la trayectoria dibujada por el usuario
function search_user_trace(){
    $.ajax({
        data:  JSON.stringify(trace),
        url:   "http://127.0.0.1:5000/query_path",
        type:  'POST',
        headers: {
            'Content-Type': 'Application/JSON'
        },
        success:  function (paths) {
            paths.forEach(function(path){
                $("#resultados").append('<li class="nav-item"><a class="nav-link" href="#")><span data-feather="user"></span>' + path[0] + '</a></li>');
                $("#resultados > li:last-child > a").click(function(){
                    get_trace(path[0]);
                });
            })
            feather.replace();
        }
    });
}

//Función que trae desde el servidor la trayectoria de la persona pasada como parámetro
function get_trace(id){
    $.get("http://127.0.0.1:5000/midpoint_trace/" + id, function(path, status){
        ctx.fillStyle = 'blue';
        path["trace"].forEach(function(entry){
            ctx.beginPath();
            ctx.arc(entry["midpoint"][0], entry["midpoint"][1], radius/2, 0, Math.PI*2);
            ctx.fill();
        })
        ctx.beginPath();
    })
}

/*function search_path(){
    $.ajax({
        data:  JSON.stringify(trace),
        url:   "http://127.0.0.1:5000/query_path",
        type:  'POST',
        headers: {
            'Content-Type': 'Application/JSON'
        },
        success:  function (data) {
            $.get("http://127.0.0.1:5000/trace" + "?pid=" + data.map(p => p[0]).join("&pid="),function(data) {
                var base_image = new Image();
                base_image.src = "data:image/png;base64," + data;
                base_image.onload = function(){
                    ctx.drawImage(this, 0, 0, canvas.width, canvas.height);
                }
            })
        }
    });
}*/

//Función que trae el frame vacío desde el server y lo dibuja en el canvas
function draw_emptyframe(){
    $.get("http://127.0.0.1:5000/scene/1", function(data, status){
        base_image = new Image();
        base_image.src = "data:image/png;base64," + data;
        base_image.onload = function(){
            ctx.drawImage(this, 0, 0, canvas.width, canvas.height);
        }
    });
}

//Función que limpia el dibujo y los resultados
function reset_canvas(){
    // Borra poligono
    clear_canvas();
    //ctx.clearRect(0, 0, canvas.width, canvas.height);
    $("#canvas").fadeOut(300, function (){
        draw_emptyframe();
        $("#canvas").fadeIn(200);
    })
    $("#resultados").empty();
    drawing_enabled = true;
}
function query_area(perimeter) {
    console.log(perimeter);
    $.ajax({
        data:  JSON.stringify(perimeter),
        url:   "http://127.0.0.1:5000/query_area",
        type:  'POST',
        headers: {
            'Content-Type': 'Application/JSON'
        },
        success:  function (response) {
            console.log(response);
        }
    });
}
function draw_area() {

    canvas.addEventListener('mousedown', point_it);
}

$("#draw_area").click(draw_area);
$("#search_path").click(search_user_trace);
$("#reset_canvas").click(reset_canvas);

// canvas.addEventListener('mousedown', engage);
// canvas.addEventListener('mouseup', disengage);
// canvas.addEventListener('mousemove', putPoint);
